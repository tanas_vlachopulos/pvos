CV 01 Uvod do vseho
===================
Manuálové stránky jsou rozděleny na více více kategorií kt. jsou číslované. pokud použijeme `man -f printf` můžeme si vylistovat v jakých kategoriích klíčové slovo je.

### Procesy
Zastaví proces kt. fork volá a vytvoří jeho potomka, který je jeho kopií a pokračuje od místa kde byl fork zavolán.

Každý proces má jiné PID. Příkaz fork() volaný v rodiči vrátí zpět PID nově vytvořeného potomka, fork() v právě vytvořeném potomkovi vrátí 0. Jenom díky tomu je možné poznat jestli jsme aktuálně v potomkovi, nebo v předkovi.

Pokud vrátí -1 došlo k chybě, mohou být vyčerpány prostředky, nebo existuje nějaký jiný důvod proč nelze potomka spustit.

> Vypis procesu `pstree -p`

Je dobrým zvykem aby si rodič převzal návratový status svého potomka, v opačném případě budou v systému vznikat zombie procesy, které budou stále blokovat prostředky OS.

Pro čekání na potomky se používá funkce `waitpid()` a `waitpid(pid)` čeká bud na libovolného nebo speficikého potomka.

Proces žádá o ukončení pomocí volání `exit()`

### Vlákna

Vlákna se chovají podobně jako procesy v tom ohledu že každé vlákno má svůj záznam v tabulce procesů, ale orginizačně vystupují jako jeden proces.

Vzhledem k tomu, že návratový kód funkce se v C vrací v globální proměnné ERRNO, která je jenom jedná, je u vláken problém s tím že by vlákna tuto proměnou přepisovaly, proto má káždé vlákno svoji vlastní globální proměnou ERRNO.

Existuje limit kolik může v rámci jednoho procesu existovat vláken. Existuje taky limit jak moc za určitou dobu může proces vláken vytvořit.

Důvod je ten že prosem má pro vlákna nějakou mapovací tabulku a ta může dojít pokud si proces korektně nevyzvedává návratová vlákna vláken.

Tento problém lze vyřešit tím že se provede **detach** vlákna, ale pak se už nemůžu dostat k návratovému kódu vlákna.

> Těmto čekajícím vláknům se neříká zombie vlákna ale princip je podobný

Obdoba detache je setsid() kde vlákno odpojí od svého proesu.

Po odpojení nelze již vlákna joinovat, ani z nich přebírat návratové hodnoty, nicméně, proces nemůže skončit dokud neskončí všechna vlákna kt. proces vytvořil.

# 02 Signaly

Signaly jsou maskovatelne a nemaskovatelne. Maskovatelnym signalum se muze proces branit, o maskovanych signalech rozhoduje system.

* sigterm 15 - donuti proces aby se sam zabil
* sigkill 9 - system proces zabije

```
# send signal
kill -s 9 process_id

# list processes
kill -l
```

ctrl+s a ctrl+q umoznuji bezici proces zastavovat klavesnici v terminalu

### sigaction

funkce alarm a sleep by nemeli byt pouzivate spolecne

Signal se muze puzit k modifikaci nebo vraceni dat beziciho procesu, treba `SIGUSR1` u utility dd vraci v periodickych intervalech status procesu dd. dd se povesi na signal SIGUSR1 a v vejakych intervalech vyvola volani funkce kt. vrati data.

### printf

Funkce printf je klasicka C funkce, pokud pomoc nic vypisujeme do terminalu, musime si dat pozor, ze terminalovy buffer vypusuje vystup teto funkce pouze ve chvili kdy se vytiskne konec radku. Nicmene pomoci funkci fflush, popripade pomoci nastaveni bufferu `setbuf` lze buffer vyprazdnit primo, nebo nastavit terminal aby nebuffroval.

`dup2` utilita kt. duplikuje souborovy deskriptor

### souborovy deskriptor

**fcntl** nastavovani priznaku souboru

**ioctl** cteni ze specialnich zarizeni

> **Tasks**
> 2. zabrzdit potomka getcharem, nebo nejakym waitem a implementovat v tem signal handler
> 

CV03 Blokujici a neblokujici operace
=====================================

Pomoci fctl lze nastavit blokujici a neblokujici rezim vstupu a vystupu. U vstupu to lze vyuzit tak ze vypnene blokujici rezim a ve smycce cekame na prichozi data.

Pokrocilejsi pristup jsou fce **select**, ktera dokaze z mnozifi file deskriptoru vybrat ty kt. obsahuji data. Pokud nastavime parametr timeout na NULL funguje v blokujicim rezimu, pokud nastavime timeout, tak ceka po urcenou dobu.

> pomoci *tty* zjistime jake cislo ma konkretni terminal
> 

Terminal defaulte bufferuje vstup po radcich, pokud chceme tuto vlastnost vypnout a pouzivat nebuferovany terminal v tzv. kanonickem rezimu pouzijeme 
`stty -F /dev/tty -icanon`

> nacteni vstupu po znacich viz kniha str 558
> 

### ukol
nacist vstup v urcitem timeoutu pomoci 3 runych metod: neblokujici, selectem a pollem

gettimeofday

CV04 Asynchronni volani
===========================

Proces neni nutne prepina do stavu blocking, ale pri vyrizeni io operace se zavola facllback v programu

man sigevent


CV05 Meziprocesni komunikace 
======================================

Klasicke nepojmenovane roury existuji vzdy pouze v ramci jednoho procesu, popripade jeho potomku, nezle je sdilet. Pojmenovane roury lze sdilet napric procesy, vytvori se jako soubor typu P v souborovem systemu a ostatni procecsy ji take mohou puzit.

vytvoření pojmenované roury v systému:
``` sh
mkfifo /tmp/roura
```

ze systému můžeme do roury zapisovat v realtime:
``` sh
cat > /tmp/roura
```

Pojmenovane roury je nutne vzdy otevrit pro zapis a pro cteni, lze ji otevrit readwrite, ale nemelo by se to delat je v tom zmatek, lepsi je pouzivat dva handly jeden pro read a druhy pro write.

Cteni z roury zpusobuje blokovani programu proto se musi uz pri cteni zapnout flag O_NONBLOCK


cv06 Semafory
====================

### System V semafory
Semafory slouzi pro meziprocesni rizeni toku. Operace nad semafory jsou atomicke, takze zvetsovat a zmensovat semafor muze vzdy pouze jeden proces v jednu chvili.

Semafar obsahuje pouze kladne cele cislo, kdyz je v semaforu 0 a my dekrementujeme bude na dekrementaci cekat tak dlouho dokud se v semaforu neobjevi dostatecne velke cislo kt. by mohl dekrementovat.

> maunualove stranky semctl(2)
> 

Procesy cekajici na semaforu jsou ve stavu sleep, takze nedochazi k busy waitingu

> vypis semefaru v systemu `ipcs`
> 
### Posixove semafory

> napoveda `man sem_open(3)`
> 
Proces muze sahat na anonymni semafor pouze pokud je sloucasti stejneho adresniho prosotoru (ramec 1 pid)

Nepojmenovane a hlavne pojmenovane semafory musi existovat mimo dany proces

musime si dat pozor na to ze pokud si proces ukoncim bez toho aniz bych semafor korektne uzavrel, nemuzu ho pak v dalsim procesu pouzit

Pozor pokud vytvorim semafor, tak se automaticky nainicializuje, ale pokud ho pak znovu oteru tak se nevynuluje a bude tam nahodna hodnota, musim ho rucne inicializovat

cv07 Fronta zprav
========================

Fronty zprav jsou atomicke, takze se nemuse stat ze by do nej spatne zapsali 2 procesy najednou.

Zpravy ve fronte jsou odlisene od sebe, takze to teni jenom pole bytu tak jako u sdilene pameti.

Synchronizace je nepatrne pomalejsi oproti semaforu protoze se musi data ze zpravy nekam nakopirovat, ale zase je velke pridana hodnta moznost poslat ruzna data.

Kazda zprava musi povinne obsahovat long mtype, ostani polozky jsou naprosto volitelne. Ale nedava smysl pouzivate pointer, protoze v kontextu jine aplikace nedavaji pointery smysl.

Delka zpravy se pocita bez long mtypu, takze s tim musime pocita pri prijmu dat. pri nacitani musime odecist velikost longu.

### Posixova fronta

Na zacatku musi byt lomitko neni mozne ho pouzivat bez neho.

Zpravy jsou rozsekane na pevnou velikost.

Zpravam lze nastavit prioritu a ony se pak predbihaji.

Fronta je na rozdil od system V file descriptor, tak to jde handlovat selectem


08 Sokety
===================

### Socket pair
Obousmerny socket podobny roure. Dada uvnitr se nesmichaji. Je to proud bytu, takze se muze zahltit a data se mohou stratit.

Je ciste pro lokalni pouziti. Nelze ho pouzit ani pro meziprocesni komunikaci, je pouze pro rodice a jeho potomky.

### Unixovy socket

K socketu se mohou dostat pouze uzivatele prihlaseni do systemu.

Socket server musi provest nasledujici faze
1. vytvorit socket
2. udelat bind na socket
3. zacit naslouchat na portu listen
4. acceptem ziskat dostupna spojeni
5. read writem cist data
6. zavrit

Socket klietn ma mene povinosti
1. vytvori socket
2. muze ale nemusi udelat bind - bind dela pouze pokud chce specificiky port nebo IP, jinak se pouzi je nahodny port a systemova adresa
3. udela connect na server
4. read/write
5. close

CV09 Sokety pokracovani
=========================

Pokud aplikaci zarezervuje port a pak zkonci bez kerektniho uvolneni system port po nejakou dobu blokuje. Lze tedy pouzita REUSERADDRESS kt. port znovu ubtainuje.

### Data out of band
V TCP je mozne zaslat packet out off band, ktery dokaze na tcp stacku predbehnout ostatni zpravy, funguje tak jako jakysi paket s vysokou prioritou. Pouziva se k signalizaci, protoze lze odeslat jen jeden byt.

Muzeme na nej zaregistrovat signal handler, nebo ho muzeme handlovat selectem pomoci mnoziny handleru pro exeptions.

Pri pouziti selectu nemzu pouzit jeden fdset jako vstup do vice mnozin.

Pro unixove sockety nejsou implementovany OOB zpravy.

CV10
==================

### Posilani FD mezi procesy:

sendmsg():

* name = null
* namelen = 0
* iov = pole iov; jako |base|length|base|length
* iovlen = 1
* control = zacatek pameti kde lezi prvni zprava ze struktury CMSG_DATA
* controller = odkaz na data, data v struktu5e DATA nemohou obsahovat pointery, protoze v kontextu druhe aplikace uz neplati
* falg = 0


CV11
===============

instalace knihovny ssl:
sudo apt install libssl-dev

Vsechna zadani 
=======================

CV01
----------
```
1. Vytvarejte procesy kazdych 10 ms, prubezne uklizejte zombie
  viz zdrojak, pozor na waitpid a WNOHANG
  
2. potomci budou koncit korekte s navratovym kodem, nebo nahodne nejak havarie 
  (deleni 0, spatny pointer)
  pomoci wait rozlisujte, jak kdo skoncil
  
3. Napiste testovaci program, ktery overi, kolik vlaken muze mit soucasne proces.
4. Napiste testovaci program, ktery overi, kolik vlaken postupne muze proces vytvorit
  (bez join a detach)
  Po zavadeni detach nebude limit.

```
  
CV02
-----------------
```
1. Overte, jak zasahuji signaly do chovani funkce scanf (sigaction bez SA_RESTART)
2. Udrzovat seznam potomku ve vector<int>. Posilejte jim nahodny signaly USR1 a URS2.
  Potomek na USR1 skonci korektne, na USR2 havarie. 
3. Na ukonceni potomku reagujte zachycenim signalu CHLD. 
  - sledujte narustani zombie
  - vyreste ztraceni signalu (pocitat ztracene signaly)
```
  
CV03
------------
```
Nastaveni terminalu na canon/no-canon mode
  stty -F /dev/tty icanon
  stty -F /dev/tty -icanon
  
Implementujte funkci read_line
  int read_line( char *buf, int len, int timeout_ms );
  
1. pomoci select
2. pomoci poll (gettimeofday, timeradd, timersub, timercmp)
3. O_NONBLOCK (usleep( 10000);)
```

CV04
------------
```
1. Pri komunikaci s pouzitim signalu SIGIO vyuzijte obsluhu signalu rozsirenou o siginfo_t.
  Overte, kdyz budou dva potomci pomalu posilat rodici data pres dve roury, 
  ze lze rozlisit zdroj (file descriptor) dat.
  
2. Pri komunikaci pres aio overte prevzeti dat pomoci vlakna (man sigevent / SIGEV_THREAD)

3. Overte, zda je omezen pocet vlaken, ktera se mohou vytvorit (viz prvni cviceni). 
  Potomek pozila data rodici po 1 bajtu, napr. po 1 ms. (Overte vlakna pomoci pthread_self).
   
4. Vyzkousejte, zda je mozno predat aiocb do obsluhy signalu pres siginfo_t 
  a polozku sigval(_t).
  Overte s vice potomky a vice rourami, min 2. 
```
  
CV05
-------------
```
1. Zmerte rychlost preneseni 100GB data mezi procesy pomoci anonymni roury, pojmenovane roury a
  pomoci sdilene pameti, kdy sdilena pamet bude cca 1-10MB a 2 roury budou synchronizovat data. 
  Prenaset se budou inty, obe strany udelaji soucet do long.
  
2. Overte, ze data v souboru je mozne pouzit stejne pres read/write i pres mmap.
  struct data { float cisla[ 10 ], suma };
  
  Rodic vytvari postupne potomky, ne soucasne vice. Potomek nahodne precte soubor pres read, 
  nebo pres mmap, zkontroluje soucet, zmeni jedno cislo, upravi soucet a ulozi data. 
```
  
CV06
------------------
```
Napiste aplikaci simulujici plneni prepravky pro MAX vyrobku. 
Rodic bude mit minimalne 10 potomku.
Potomci plni prepravku, ta je ve sdilene pameti: struct { int pocet, vyrobky[ MAX ]; }
Kazdy potomek vzdy precte pocet, zapise svuj pid mezi vyrobky[pocet++]
Pokud potomek vlozi na posledni volne misto, musi potomci pockat na vymenu prepravky.

1. Implementace pomoci semaforu System V, rodic snizuje semafor prepracky o MAX.
  Rodic pri vyprazdneni prepravky dela statistiku, kolik vyrobku dodal ktery potomek 
  (jeho pid ve sdilene pameti). Vhodne pouzit unordered_map (hashovaci tabulka), c++11. 

2. Implementace pomoci semaforu System V, rodic vyuzije vlastnosti wait-for-zero. 

3. Implementace pomoci Posix semaforu. 
```

CV07
-------------
```
1. Prepiste priklad z minuleho cviceni tak, aby se prepravka plnila a synchoronizace
  byla zajistena pomoci fronty system V i Posix.
  
2. Implementujte frontu zprav pro vice typu zprav pomoci sdilene pameti a semaforu.

  Pocet zprav ve fronte omezen na predem dany pocet.
  Pocet typu zprav omezit napr. na 8. 
   
  struct zprava { int typ; char neco[ 12 ] };
  
  Do sdilene pameti bude potreba take umistit 8 + 1 semaforu.
  
3. Zamyslete se a vyzkousejte, zda by slo predchozi reseni upravit tak, aby typ zpravy 0 byl
  opet pro vyber zprav bez ohledu na typ. 
```
  
CV08
-------------------
```
Vytvorte socket server bez vlaken a procesu tak, aby obsluhoval soucasne vice klientu
a fungoval jako "chat" - co prijme od klienta, to posle zpet vsem klientum. 

Vyuzijte select i poll, pro evidenti klientu pouzijte vector<int> a vector<pollfd>
```

CV09
---------------------
```
1. Rozsirte socket klienta a select/poll server o OOB data obousmerne. 
2. Kdyz server prijme noveho klienta, posle vsem OOB 'N'. 
  Kdyz se minutu nic nedeje na serveru, posle klientum OOB 'T' a klienti si vypisi aktualni cas. 
3. Kdyz server prijme OOB 'L', posle vsem pripojenym klientum seznam pripojenych klientu.  
```

CV10
---------------------
```
Upravte soktetovy server pro "chat" ve variante select nebo poll. 
Rodic vytvori dva potomky, kterym bude pres 2xsocketpair predavat nove klienty.
Potomci budou prijimat zpravy jen od sudych nebo lichych deskriptoru, ale stale posilat vsem. 

Rodic resi accept, posle obema potomkum. Potomci maji select nebo poll jen na lichych/sudych 
deskriptorech. 
```

CV11
-----------------
Upravit chat na SSL


