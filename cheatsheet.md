PVOS Cheatsheet
===================

Signály
-----------

Pro signál se musí vytvoři handler, jako parametr přijímá číslo signálu:

```cpp
void mysignal(int sig)
{
    printf("this is my signal %d\n", sig);
}

```

Následně se musí handler zaregistrovat:

```cpp
struct sigaction myact;
myact.sa_handler = mysignal;
myact.sa_flags = 0; //SA_RESTART;
sigemptyset(&myact.sa_mask);
sigaction(SIGALRM, &myact, NULL); // registrace signalu SIGALRM
```
Ruční vyvolání signálu:
```cpp
kill(process_pid, SIGUSR1);
```

Signály:

* SIGUSR1, SIGURSR2 - uživatelsky dostupné signály
* SIGALRM - signál generovaný časovačem funkce alarm
* SIGIO - signál při uživatelském vstupu a výstupu

Vice info v **man 7 signal**

I/O
----------------

Poll a select slouzi ke hlidani file descriptoru pokud se blokuji program dokud na FD nedojde ke zmene.

### Poll

`int poll(struct pollfd *fds, nfds_t nfds, int timeout );`

* fds = list of file pollfd structure contains file descriptor and event
* nfds = count of file descriptors
* timeout = timeout in ms, 0 = infinite time

If poll timeouted return 0.

Poll events can be set for mor events with bit operation OR `pollset[0].events = POLLIN | POLLPRI;`

More info **man poll**.

```cpp
struct pollfd poll_set[30]; // observed descriptors
memset(poll_set, 0, sizeof(poll_set));

poll_set[0].fd = file_descriptor; 
poll_set[0].events = POLLIN; // event

int fd_count = 1;

int poll_ret = poll(poll_set, fd_count, 0);

if (poll_set[0].revents & POLLIN) {
    // reaction on event	
}
```

### Select 

```
int select(int nfds, fd_set *readfds, fd_set * writefds, fd_set * exceptfds , struct timeval *timeout );
```

* nfds = nejvetsi file descriptor ze skupiny sledovanych sescriptoru + 1, pokud mame dynamicke pole descriptoru musime vzdy najit maximum
* readfds = sledovani udalosti can read
* writefds = sledovani udalosti can write
* exceptfds = sledovani udalosti error nebo specialnich zprav jako OOB
* timeout = akceptuje struktury timeval, pokud se udalost stane pred vyprsenim casu obsahuje timeout zbyvajici cas, pokud cas vyprsi obsahuje timeout 0
* return = 0 pokud vyprsi cas

```
struct timeval timeout;
timeout.tv_sec = 10;
timeout.tv_usec = 0;
```

```cpp
fd_set wait_set;
FD_ZERO(&wait_set);
FD_SET(file_descriptor, &wait_set); // set for each file desc.

int res_sel = select(max_fd + 1, &wait_set, NULL, NULL, NULL);

if (FD_ISSET(file_descriptor, &wait_set)) {
    // reaction on event
}
```

Sokety
--------------
### Socket pair

Obousmerny soket podobny roure, ktery muze fungovat pouze v ramci rodice a jeho potomku.

Kazda ucastnik dostane jeden konec a muze do nej zapisovat nebo z nej cist.

```cpp
int socket_pairs[2][2];
for (int c = 0; c < 2; c++)
{
    socketpair(AF_UNIX, SOCK_STREAM, 0, socket_pairs[c]);
}
```
### Unixovy socket

### AF_INET soket

Uzivatel muze bindovat pouze sokety vetsi nez 1024 a musi se konvertovat pres funkci *hton*. Bindovani socketu neni povine, pokud se neudela pouzije se nahodny port.

Pokud sluzbu na danem portu vypneme je port jeste nekolik minut rezervovan systemem, abychom jej uvolnili lze pouzit funkci *setsockopt*, ktera port opet ziska pro nas bind.

#### Server:

```cpp
sockaddr_in inadr;
inadr.sin_family = AF_INET;
inadr.sin_port = htons(11111); // bind port 11111
inadr.sin_addr.s_addr = INADDR_ANY;

int sd = socket(AF_INET, SOCK_STREAM, 0);

// reuse port 11111 when is still locked with previos process
int opt = 1;
if (setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) < 0) {
    printf("Unable to set socket option!\n");
}


// bind port and address
int err = bind(sd, (sockaddr *)&inadr, sizeof(inadr));
if (err < 0) {
    printf("bind failed\n");
}

// listen on port
if (listen(sd, 1) < 0) {
    printf("cannot listen\n");
    close(sd);
    exit(1);
}

// wait for client
int newsock = accept(sd, (sockaddr *)&peeradr, &len_adr);
// do some socket business
close(newsock);
close(sd);
```

#### Client:

```cpp
addrinfo ai_req, *ai_ans;
bzero(&ai_req, sizeof(ai_req));
ai_req.ai_family = AF_INET;
ai_req.ai_socktype = SOCK_STREAM;

int get_ai = getaddrinfo(host, NULL, &ai_req, &ai_ans);
if (get_ai)
{
    log_msg(LOG_ERROR, "Unknown host name!");
    exit(1);
}

sockaddr_in cl_addr = *(sockaddr_in *)ai_ans->ai_addr;
cl_addr.sin_port = htons(port);
freeaddrinfo(ai_ans);

// socket creation
int sock_server = socket(AF_INET, SOCK_STREAM, 0);
if (sock_server == -1)
{
    log_msg(LOG_ERROR, "Unable to create socket.");
    exit(1);
}

// connect to server
if (connect(sock_server, (sockaddr *)&cl_addr, sizeof(cl_addr)) < 0)
{
    log_msg(LOG_ERROR, "Unable to connect server.");
    exit(1);
}
```

Pipes
-----------------
### Nepojmenovane pipy

Jedna se o jednosmerny komunikacni kanal sdileny mezi vramci procesu a jeho potomku. 

Pipe **0** slouzi pro **cteni** a pipe **1** pro **zapis**.

```cpp
int pipes[10][2]; // create 10 pipes

for (int i = 0; i < 10; i++) {
	pipe(pipes[i]); // init pipes
}

write(pipes[0][1], "ahoj", 5); // write to pipe

char buff[16];
int ret = read(pipes[0][0], buff, sizeof(buff));

close(pipes[0]);
```

### Pojmenovane roury

Jednosmerny komunikacni kanal, ktery muze byt sdilen mezi vice procesy a lze do nej zapisovat a cist z file systemu.

Roura je reprezentovana souborem v systemu a v linuxu ji lze vytvorit pomoci `mkfifo nazev_roury` a zapisovat do ni lze pomoci `cat > nazev_roury`

Roury se oteviraji bud pro cteni nebo pro zapis, neni korektni je zapisovat jako readwrite protoze muze dochazet ke zmatkum, lepsi je otevrit cteci a sapisovaci instanci.

Cteni z roury blokuje program proto se pouziva casto v rezimu O_NONBLOCK spolecne se selectem a pollem.

```cpp
// open for read in nonblocking mode
int tube_read = open("/tmp/roura", O_RDONLY | O_NONBLOCK); 

// switch back to blocking mode
fcntl(tube_read, F_SETFL, fcntl(tube_read, FGETFL) & ~O_NONBLOCK); 

char buff[255];
int ret = read(tube_read, buff, sizeof(buff));

// open pipe for write
int tube = open("/tmp/roura", O_WRONLY);
write(tube, buff, sizeof(buff));
```

Sdílená paměť
----------------

Sdilena pamet je blok pameti s libovolnym obsahem kt. mohou upravovat rodic i jeho potomci. Pro vytvoreni bloku sdilene pameti slouzi **mmap**:

```cpp
void *mmap(void *addr, size_t length, int prot, int flags, int fd, off_t offset);
```

* addr = je vzdy NULL, system si sam vybere kam ho namapuje
* lenght = velikost pameti kt. chceme naalokovat
* prot = vetsinou **PROT_READ | PROT_WRITE**
* flags = urcuje jestli bude jen pro dany proces, nebo sdilene, pro sdilene pouzit **MAP_SHARED | MAP_ANONYMOUS**
* fd = je deskriptor nejakeho souboru, vetsinou davat hodnotu **-1**
* offset = vetsinou **0**

#### Anonymni sdilena pamet

Muse byt sdilena pouze v ramci rodice a potomku.

```cpp
// vytvori blok sdilene pameti o velikosti 100 intu
int *data = (int *)mmap(NULL, 100 * sizeof(int), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
// promazani pameti na 0
memset(data, 0, 100 * sizeof(int));
```

#### V pameti mapovany soubor

```cpp
int data = open("data.dat", O_CREAT | O_RDWR, 0660);
ftruncate(data, 100); // nafoukne soubor na pozadovanou velikost

// vytvori sdilenou pamet odvozenou od FD data
char *ptrdata = (char *) mmap(NULL, 100, PROT_READ | PROT_WRITE, MAP_SHARED, data, 0);
strcpy(ptrdata, "Blahoslaveni budiz oraj\n"); // prekopiruje text do pameti
msync(ptrdata, 100, MS_SYNC); // provede synchronizaci dat
    
```

#### Sdilena pamet POSIX

Sdilena pamet podobna te anonymni, ale muze byt sdilena napric procesy. Postup je stejny jako u mapovaneho souboru:

1. Pomoci **shm_open** vytvorime pametovy soubor.
2. Nafoukneme jej pomoci **ftruncate**.
3. Vytvorime s nim sdilenou pamet pomoci **mmap**.


Semafory
----------

Semafory jsou základní synchronizační prvek v systému, zajišťují že do kritické sekce nevstoupí více než určené množštví procesů.

### System V semafor

Semafor je identifikovan unikatnim ID. Jedno ID muze obsahovat vice semaforu. Semafory se vytvari pomoci **semget**.

```cpp
int semget(key_t key, int nsems, int semflg);
```

* key = unikatni klic napr.: **0xbeefbeef**
* nsems = pocet semaforu kt. bude vytvoren
* semflg = definuje systemova opravneni pro semafor a to zda ma byt vytvoren, bezne pouzit **IPC_CREAT | 0600**

Se semaforem se nasledne pracuje pomoci funkce **semop**.

```cpp
int semop(int semid, struct sembuf *sops, unsigned nsops);
```

* semid = identifikator semaforu, tedy to co vrati funkce semget
* sops = operace se semaforem, predava se specialni ridici struktura **sembuf**
* nsops = pocet semaforu v poli

To jakym semaforem se bude manipulovat a co se ma provest urcuje struktura **sembuf**.

```cpp
struct sembuf
{
	unsigned short sem_num; /* semaphore number */
	short sem_op; /* semaphore operation */
	short sem_flg; /* operation flags */
};
```

* sem_num = poradove cislo semaforu
* sem_op = operace kt. se ma se semaforem provest, +- hodnota
* sem_flg = ?

Semafor muze pracovat ve dvou rezimech:
1. Pokud se snazime odecist hodnotu ze semaforu blokuje tak dloho dokud v nem neni dostatecne velka hodnota kt. jde odecist (semafor nemuze obsahovat zapornou hodnotu). Pokud je tedy v semaforu 5 a my se snazime odecist -6 bude semop blokujici. Pokud je v semaforu 6 a my pomoci semops odecitame -6 nedojde k zablokovani.
2. Pokud nastavime strukturu sembuf na same 0 *{0, 0, 0}* bude naopak semafor blokovat tak dlouho dokud se v nem neobjevi 0.

```cpp
int sem_put_item = semget(0xcafecafe, 1, IPC_CREAT | 0600); // vytvori semafor
semctl(sem_put_item, 0, SETVAL, 1); // nastavi semafor na hodnotu 1

sembuf up = {0, 1, 0}; // zvysuje hodnotu semaforu 0 o hodnotu 1
sembuf down = {0, -1, 0}; // snizuje hodnotu semaforu 0 o hodnotu 1

// zamkne semafor - ubere hodnotu 1, v semaforu je tedy 0 a blokuje
semop(sem_put_item, &down, 1);
// odemkne semafor - prida hodnotu 1 a muze tedy probehnout dalsi semop -1
semop(sem_put_item, &up, 1); 

// ceka na semop dokud se v semaforu neobjevi 0
sembuf empty = {0, 0, 0};
semop(sem_box, &empty, 1);
```

### Posix semafor

Posixove semafory mohou byt pojmenovane nebo anonymni. Anonymni semafory se pouzivaji 2 zpusoby, pokud chceme synchronizovat jenom vlakna, nebo neco v hlavnim procesu muze byt semafor globalni promenna, pokud se ale bude pouzivat mezi rodici a potomky musi byt ve sdilene pameti.

#### Pojmenovany semafor

```cpp
sem_t * sem_open (const char *name, int oflag, mode_t mode, unsigned int value);
```

* name = cesta k semaforu, musi zacinat lomitkem!
* oflag = vytvareci flagy typicky **O_CREAT | O_RDWR**
* mode = systemova opravneni **0660**
* value = pocatecni hodnota

Semafor se musi jeste nainicializovat

```cpp
int sem_init (sem_t *sem, int pshared, unsigned int value);
```

* sem = to co vrati sem_open
* pshared = 0 pokud se jedna o nestdileny nepojmenovany semafor, 1 pokud ma byt sdileny
* value = pocatecni hodnota

```cpp
// vytvori novy semafor a nastavi mu hodnotu 1
sem_t *sem_put_item = sem_open("/sem_put_item", O_CREAT | O_RDWR, 0660, 1);
// inicializuje semafor jako sdileny a opet mu nastavi pocatecni hodnotu 1
sem_init(sem_put_item, 1, 1);

// znizi hodnotu o 1 a blokuje pokud je v semaforu 0 a nejde nic odecist
sem_wait(sem_put_item); 
// zvisi semafor o 1
sem_post(sem_put_item);
// snizi hodnotu o 1 ale neblokuje pokud je v semaforu 0
sem_trywait(sem_put_item);
```

Fronty zprav
--------------

Fronta zprav je spolehlyvy prostredek obosmerne komunikace mezi procesy, nedochazi k zadnemu zahazovani zprav a pristup je atomicky, takze muse slouzit i pro synchronizaci. Fronty mohou byt jen pojmenovane.

### Fronta System V

Fronta se vytvari na zaklade jedinecneho identifikatoru.

```cpp
int msgget(key_t key, int msgflg);
```

* key = unikatni ID, napr.: **0xbeefbeef**
* msgflg = flagy, typicky: **IPC_CREAT | 0600**

Frontou se musi predavat zpravy ve specifickem formatu, musi to byt struktura kt. obsahuje prinejmensim jednu polozku **type** typu **long**, uzivatel do ni pak musi priradit kladne cislo kt. identifikuje typ zpravy, dale musi obsahovat nejaka data nenulove delky.

```cpp
int msgsnd(int msqid, const void *msgp, size_t msgsz, int msgflg);
```

* msqid = to co vrati msgget
* msgp = struktura dat kt. chceme sposlat
* msgsz = velikost dat
* msgflg = specialni flagy treba IPC_NOWAIT

Struktura **queue** obsahujici povinny typ a libovolna data.
```cpp
struct queue
{
    long type;
    int freespace;
};
```

**!!!** pozor type zpravy se do vysledne velikosti zpravy nepocita, takze pri **msgsnd** musime velikost typu odecist, tedy 1x long

Pri prijimani zprav si muzeme vybrat jaky typ chceme, pokud si vyberem 0 dostaneme prvni na rade z nehlede na typ. Pro cteni se pouziva **msgrcv** ktery blokuje chod pokud se ve fronte nenachani zadna zprava k vyzvednuti.

```cpp
ssize_t msgrcv(int msqid, void *msgp, size_t msgsz, long msgtyp, int msgflg);
```

* msgid = to co vrati msgget
* msgp = odkaz na strukturu do kt. se ulozi data
* msgsz = velikost nactenych dat
* msgtyp = typ dat kt. se budou nacitat
* msgflg = speciali flagy, zase IPC_NOWAIT

Pomoci **msgctl** lze zjistit stav fronty bez toho aby se cokoliv vytahovalo, viz **man msgctl**.

```cpp
// vytvoreni fronty
int msg = msgget(0xbeefbeef, IPC_CREAT | 0600);

queue insert_lock; // vytvoreni struktury typu queue
insert_lock.type = 1; // urceni typu
insert_lock.freespace = 10; // vlozeni uzivatelskych dat
msgsnd(msg, &insert_lock, sizeof(queue) - sizeof(long), 0); // odeslani zpravy

// cteni zpravy typu 1 do struktury insert_lock
msgrcv(msg, &insert_lock, sizeof(queue) - sizeof(long), 1, 0);
```

### Fronta Posix

Opet velmi podobna fronte v System V. Fronta je reprezentovana klasickym FD a lze na ni pouzit poll a select. Fronta se vytvari nasledovne:

```cpp
mqd_t mq_open(const char *name, int oflag, mode_t mode, struct mq_attr *attr);
```

* name = jmeno fronty, **!!!** musi zacinat lomitkem /
* oflag = flagy typicky **O_RDWR | O_CREAT**
* mode = systemova prava **0660**
* attr = parametry fronty, muze byt **NULL** pak jde o default, nebo muzeme nastavit pocty zprav atd. pomoci struktury **mq_attr**

Struktura mq_attr:

```cpp
struct mq_attr ma;
ma.mq_flags = 0;
ma.mq_maxmsg = 200; // max pocet zprav
ma.mq_msgsize = sizeof(int); // velikost zpravy
ma.mq_curmsgs = 0;
```

Zapis a cteni do fronty **mq_send** a **mq_receive** maji stejnou signaturu:

```cpp
int mq_send(mqd_t mqdes, const char *msg_ptr, size_t msg_len, unsigned msg_prio );
```
* mqdes = FD fronty
* msg_ptr = odkaz na data
* msg_size = velikost dat (neni treba odpocitavat zadny ty jako u system V)
* msg_prior = priorita zprav, ty z vysi prioritou preskoci ty z nizsi, default 0

```cpp
struct mq_attr ma;
ma.mq_flags = 0;
ma.mq_maxmsg = MAX_LOAD;
ma.mq_msgsize = sizeof(int);
ma.mq_curmsgs = 0;
mqd_t mq_insert = mq_open("/fronta_insert2", O_RDWR | O_CREAT, 0600, &ma);

// odeslani zpravy
int msg_insert = 10;
mq_send(mq_insert, (char *)&msg_insert, sizeof(int), 0);

// prijimani zpravy
int insert_lock;
mq_receive(mq_insert, (char *)&insert_lock, sizeof(int), 0);
```