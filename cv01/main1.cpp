#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main()
{
	int pid = fork();
	if ( pid )
	{
		printf( "ja jsem rodic %d\n", getpid() );
		printf( "cekam na potomka %d\n", pid );
		wait( NULL );
		printf( "potomek skoncil\n" );
		getchar();
	}
	else
	{
		printf( "ja jsem potomek %d\n", getpid() );
		sleep( 5 );
	}
	return 8;
}
