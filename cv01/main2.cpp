#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pthread.h>

void *vlakno( void *ptr )
{
	char *str = ( char * ) ptr;
	printf( "ja jsem vlakno %s\n", str );
	const char *retstr = "navratovy kod";
	getchar();
	return strdup( retstr );
}

int main()
{
	pthread_t th_id;
	const char *jmeno = "karel";
	pthread_create( &th_id, NULL, vlakno, ( void * ) jmeno );
	void *ret;
	pthread_join( th_id, &ret );
	printf( "proces konci %s\n", ( char * ) ret );
}
