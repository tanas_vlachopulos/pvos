#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main()
{
	int pid = -1;
	for ( int i = 0; i < 10; i++ )
		if ( ( pid = fork() ) == 0 ) break;
	if ( pid ) // rodic
	{
		int ret;
		while ( ( ret = wait( NULL ) ) != -1 ) printf( "wait %d\n", ret );

	}
	else // potomek
	{
		srand( getpid() );
		usleep( 100000 * ( rand() % 50 ) );
		printf( "konci potomek %d\n", getpid() );
	}
}
/*
while ( 1 )
  fork()
  if ( rodic/potomek )...
  usleep( kolim ms)
  while ( waitpid( -1, NULL, WNOHANG ) ??
*/
