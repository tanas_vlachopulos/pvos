#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main()
{
    while(1)
    {
        int pid = -1;

        usleep(100000);
        pid = fork();
        srand(getpid());

        if (pid > 0) // parent 
        {
            // usleep(100000 * (rand() % 50));
            int kidpid = waitpid(-1, NULL, WNOHANG);
            if (kidpid > 0)
            {
                printf("Geting return code from child: %d\n", kidpid);
            }
        }
        else if (pid == 0) // child
        {
            printf("Creating child %d\n", getpid());
            usleep(100000 * (rand() % 50));
            printf("Child %d finished.\n", getpid());
            return 0;
        }
        else 
        {
            return 0;
        }
    }
}