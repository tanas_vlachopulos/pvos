#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int sleeptime = 1000000;

int main()
{
    while(1)
    {
        int pid = -1;

        usleep(sleeptime);
        pid = fork();
        srand(getpid());

        if (pid > 0) // parent 
        {
            // usleep(100000 * (rand() % 50));
            int status;
            int kidpid = waitpid(-1, &status, WNOHANG);
            if (kidpid > 0)
            {
                printf("Return code from child %d is: %d\n", kidpid, WEXITSTATUS(status));
                printf("Child %d is terminated %s\n", kidpid, (WCOREDUMP(status)? "with errors":"normaly"));
            }
        }
        else if (pid == 0) // child
        {
            printf("Creating child %d\n", getpid());
            usleep(sleeptime * (rand() % 50));
            
            int retval = rand() % 5;
            printf("Child %d finished with return value %d.\n", getpid(), retval);
            
            // generate random error
            int wormhole = 31415 / retval;

            return retval;
        }
        else 
        {
            return 0;
        }
    }
}