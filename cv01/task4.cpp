#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <pthread.h>

void *thread(void *args)
{
	getchar();
}

int main()
{
	int counter = 0;
	int status = 0;

	pthread_t t_id;

	while (status == 0)
	{
		status = pthread_create(&t_id, NULL, thread, NULL);
		counter++;
	}
	printf("Maximal count of threads total: %d\n", counter);

	return 0;
}