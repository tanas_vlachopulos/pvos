#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

void mysig(int sig)
{
    printf("this is my signal %d\n", sig);
}

int main()
{

    struct sigaction myact;
    myact.sa_handler = mysignal;
    myact.sa_flags = 0; //SA_RESTART;
    sigemptyset(&myact.sa_mask);
    sigaction(SIGALRM, &myact, NULL);
    alarm(5);
    sleep(10);
    char c;
    int r = read(0, &c, 1);
    printf("r=%d errno=%d (%s)\n", r, errno, strerror(errno));

    // int counter = 0;
    // while (1)
    // {
    //     if
    //         counter == 10
    //         {
    //             fflush();
    //             counter = 0;
    //         }

    //     printf("ahoj");
    //     usleep("100");
    // }

    return 0;
}