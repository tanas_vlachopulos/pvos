// 2. Udrzovat seznam potomku ve vector<int>. Posilejte jim nahodny signaly USR1 a URS2.
// Potomek na USR1 skonci korektne, na USR2 havarie. 

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <vector>

int sleeptime = 250000;

void my_usr1(int sig)
{
    printf("Usr1 callback in child %d\n", getpid());
    exit(0);
}

void my_usr2(int sig)
{
    printf("Usr2 callback in child %d\n", getpid());
    exit(1);
}


int main()
{
    std::vector<int> childs;

    while(1)
    {
        int pid = -1;

        usleep(sleeptime);
        pid = fork();
        srand(getpid());

        if (pid > 0) // parent 
        {
            // usleep(100000 * (rand() % 50));
            printf("this is PID in parrent %d \n", pid);
            childs.push_back(pid);            

            printf("child count %d\n", childs.size());
            if (childs.size() > 10)
            {
                int position = rand() % childs.size();
                
                printf("sending signal to child %d\n", childs.at(position));

                srand(pid);
                int randswitch = rand() % 2;
                if (randswitch == 0) 
                {
                    kill(childs.at(position), SIGUSR1);      
                }
                else
                {
                    kill(childs.at(position), SIGUSR2);
                }
                
                childs.erase(childs.begin() + position);
            }

            int kidpid, status;
            while(kidpid = waitpid(-1, &status, WNOHANG))
            {
                printf("Return code from child %d is: %d\n", kidpid, WEXITSTATUS(status));
            }

        }
        else if (pid == 0) // child
        {
            printf("Creating child %d\n", getpid());
            
            // register USER1 signal
            struct sigaction my_act_usr1;
            my_act_usr1.sa_handler = my_usr1;
            my_act_usr1.sa_flags = 0; //SA_RESTART;
            sigemptyset(&my_act_usr1.sa_mask);
            sigaction(SIGUSR1, &my_act_usr1, NULL);

            // register USER2 signal
            struct sigaction my_act_usr2;
            my_act_usr2.sa_handler = my_usr2;
            my_act_usr2.sa_flags = 0; //SA_RESTART;
            sigemptyset(&my_act_usr2.sa_mask);
            sigaction(SIGUSR2, &my_act_usr2, NULL);

            // usleep(sleeptime * (rand() % 50));
            
            // int retval = rand() % 5;
            // printf("Child %d finished with return value %d.\n", getpid(), retval);
            
            // // generate random error
            // int wormhole = 31415 / retval;

            // return retval;

            getchar();
        }
        else 
        {
            return 0;
        }
    }
}