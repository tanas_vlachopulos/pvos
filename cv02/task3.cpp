/*
3. Na ukonceni potomku reagujte zachycenim signalu CHLD. 
- sledujte narustani zombie
- vyreste ztraceni signalu (pocitat ztracene signaly)
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

int sleeptime = 10;
int child_count = 0;
int zombie_total = 0;
int handle_count = 0;

void signal_handler(int signal)
{
    printf("\nHandle (%d childs created)\t", child_count);
    handle_count++;

    int counter = 0;
    int kidpid, status;
    while ((kidpid = waitpid(-1, &status, WNOHANG) > 0))
    {
        // printf("catching zombie %d\n", kidpid);
        counter++;
    }
    counter--; // remove from zombie counter child which send signal
    zombie_total += counter;
    printf("%d zombies found\t %f average\n", counter, ((double)zombie_total / (double)handle_count));
}

int main()
{
    int parent_pid = getpid();
    int registered = 0;
    int pid = -1;

    while (1)
    {
        usleep(sleeptime / 3);

        // register signal SIGCHILD
        struct sigaction my_act_usr1;
        my_act_usr1.sa_handler = signal_handler;
        my_act_usr1.sa_flags = 0; //SA_RESTART;
        sigemptyset(&my_act_usr1.sa_mask);
        sigaction(SIGCHLD, &my_act_usr1, NULL);

        pid = fork();

        if (pid > 0) // parent
        {
            child_count++;
        }
        else if (pid == 0) // child
        {
            // printf("This is child %d\n", getpid());
            printf(".");
            usleep(sleeptime);

            // kill(parent_pid, SIGUSR1);
            exit(0);
        }
    }
}