#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>

/*
1. Overte, jak zasahuji signaly do chovani funkce scanf (sigaction bez SA_RESTART)
2. Udrzovat seznam potomku ve vector<int>. Posilejte jim nahodny signaly USR1 a URS2.
  Potomek na USR1 skonci korektne, na USR2 havarie. 
3. Na ukonceni potomku reagujte zachycenim signalu CHLD. 
  - sledujte narustani zombie
  - vyreste ztraceni signalu (pocitat ztracene signaly)
*/

void mysignal(int sig)
{
    printf("this is my signal %d\n", sig);
}

int main()
{
    struct sigaction myact;
    myact.sa_handler = mysignal;
    myact.sa_flags = 0; //SA_RESTART;
    sigemptyset(&myact.sa_mask);
    sigaction(SIGALRM, &myact, NULL);
    alarm(5);
    sleep(10);
    char c;
    int r = scanf(0, &c, 1);
    printf("r=%d errno=%d (%s)\n", r, errno, strerror(errno));

    return 0;
}