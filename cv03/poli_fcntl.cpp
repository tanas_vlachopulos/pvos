#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>

#define LEN 10


int main()
{
	int roury[ LEN ][ 2 ];
	for ( int i = 0; i < LEN; i++ )
		pipe( roury[ i ] );

	int pid = -1;
	int i = 0;
	for ( ; i < LEN; i++ )
	{
		if ( ( pid = fork() ) == 0 ) break;
	}

	if ( !pid ) // potomci
	{
		for ( int j = 0; j < LEN; j++ )
		{
			if ( j == i ) continue;
			close( roury[ j ][ 0 ] );
			close( roury[ j ][ 1 ] );
		}
		srand( getpid() );
		while ( 1 )
		{
			char buf[ 128 ];
			int cas = rand() % 2000;
			printf( "(%d) bude cekat %d ms\n", getpid(), cas );
			usleep( cas * 1000 );
			sprintf( buf, "(%d) %d\n", getpid(), rand() % 100 );
			write( roury[ i ][ 1 ], buf, strlen( buf ) );
			printf( "(%d) odeslal\n", getpid() );
		}
	}

	for ( int j = 0; j < LEN; j++ )
	{
		int fl = fcntl( roury[ j ][ 0 ], F_GETFL );
		fcntl( roury[ j ][ 0 ], F_SETFL, fl | O_NONBLOCK );
	}
	while ( 1 )
	{
		for ( int j = 0; j < LEN; j++ )
		{
			char buf[ 128 ];
			int ret = read( roury[ j ][ 0 ], buf, sizeof( buf ) );
			if ( ret < 0 )
			{
				fprintf( stderr, "Nejsou data\n" );
				usleep( 1000 );
			}
			else write( 1, buf, ret );
		}
	}
}
