	#include <stdio.h>
	#include <stdlib.h>
	#include <unistd.h>
	#include <errno.h>
	#include <string.h>
	#include <signal.h>
	#include <sys/types.h>
	#include <sys/stat.h>
	#include <fcntl.h>
	#include <sys/wait.h>
	#include <sys/select.h>
	#include <sys/param.h>

	#define LEN 10


	int main()
	{
		int roury[ LEN ][ 2 ];
		for ( int i = 0; i < LEN; i++ )
			pipe( roury[ i ] );

		int pid = -1;
		int i = 0;
		for ( ; i < LEN; i++ )
		{
			if ( ( pid = fork() ) == 0 ) break;
		}

		if ( !pid ) // potomci
		{
			for ( int j = 0; j < LEN; j++ )
			{
				if ( j == i ) continue;
				close( roury[ j ][ 0 ] );
				close( roury[ j ][ 1 ] );
			}
			srand( getpid() );
			while ( 1 )
			{
				char buf[ 128 ];
				int cas = rand() % 2000;
				printf( "(%d) bude cekat %d ms\n", getpid(), cas );
				usleep( cas * 1000 );
				sprintf( buf, "(%d) %d\n", getpid(), rand() % 100 );
				write( roury[ i ][ 1 ], buf, strlen( buf ) );
				printf( "(%d) odeslal\n", getpid() );
			}
		}

		while ( 1 )
		{
			fd_set cteni;
			FD_ZERO( &cteni );
			int nejvetsi = 0;
			for ( int j = 0; j < LEN; j++ )
			{
				FD_SET( roury[ j ][ 0 ], &cteni );
				nejvetsi = MAX( nejvetsi, roury[ j ][ 0 ] );
			}
			timeval tout = { 0, 100000 };
			int sel = select( nejvetsi + 1, &cteni, NULL, NULL, &tout );
			fprintf( stderr, "sel=%d %ld.%ld\n", sel, tout.tv_sec, tout.tv_usec );
			if ( sel > 0 )
			for ( int j = 0; j < LEN; j++ )
			{
				if ( FD_ISSET( roury[ j ][ 0 ], &cteni ) )
				{
					char buf[ 128 ];
					int ret = read( roury[ j ][ 0 ], buf, sizeof( buf ) );
					if ( ret < 0 )
					{
						fprintf( stderr, "Nejsou data\n" );
						usleep( 1000 );
					}
					else write( 1, buf, ret );
				}
			}
		}
	}
