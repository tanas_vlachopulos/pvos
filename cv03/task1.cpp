/*
Nastaveni terminalu na canon/no-canon mode
  stty -F /dev/tty icanon
  stty -F /dev/tty -icanon
  
Implementujte funkci read_line
  int read_line( char *buf, int len, int timeout_ms );
  
1. pomoci select
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <sys/param.h>

#define LEN 10

int read_line(char *buffer, int len, int timeout)
{
    timeval tout = {timeout, 0};
    
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds);
    int counter = 0;

    while (1)
    {
        int sel = select(STDIN_FILENO + 1, &fds, NULL, NULL, &tout);
        // printf("%d\n", sel);

        if (sel > 0)
        {
            char ch;
            if (read(STDIN_FILENO, &ch, 1) > 0)            
            {
                // printf("char registered %c\n", ch);
                if (ch == 10 || counter >= len)
                    return 0;

                buffer[counter] = ch;
                counter++;
            }
        }
        else if (sel == 0)
        {
            printf("Read line timeout\n");
            return 0;
        }
        else 
        {
            printf("Read line Error\n");
            return 1;
        }

        usleep(100);
    }
}

int main()
{
    char *input_char;
    int result;

    char buff[255] = {0};
    read_line(buff, sizeof(buff), 3);
    printf("%s\n", buff);

}