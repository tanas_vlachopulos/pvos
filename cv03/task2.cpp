/*
Nastaveni terminalu na canon/no-canon mode
  stty -F /dev/tty icanon
  stty -F /dev/tty -icanon
  
Implementujte funkci read_line
  int read_line( char *buf, int len, int timeout_ms );
  
2. pomoci poll (gettimeofday, timeradd, timersub, timercmp)
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/param.h>
#include <poll.h>

int read_line(char *buffer, int len, int timeout)
{
    int counter = 0;
    struct pollfd pfd[1];
    pfd[0].fd = STDIN_FILENO;
    pfd[0].events = POLLIN;

    struct timeval t_end, t_now, t_diff;
    gettimeofday(&t_end, NULL);
    t_end.tv_sec += timeout;

    while (1)
    {
        gettimeofday(&t_now, NULL);
        timersub(&t_end, &t_now, &t_diff);
        // printf("%d\n", t_diff.tv_sec * 1000);
        
        int sel = poll(pfd, 1, t_diff.tv_sec * 1000);
        // printf("%d\n", sel);
        if (sel > 0)
        {
            char ch;
            if (read(STDIN_FILENO, &ch, 1) > 0)
            {
                // printf("char registered %c\n", ch);
                if (ch == 10 || counter >= len)
                    return 0;

                buffer[counter] = ch;
                counter++;
            }
        }
        else if (sel == 0)
        {
            printf("\nTimeout\n");
            return 0;
        }
    }
}

int main()
{
    char buff[255] = {0};
    read_line(buff, sizeof(buff), 3);
    printf("Inserted text: %s\n", buff);
}