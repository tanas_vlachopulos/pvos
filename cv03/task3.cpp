/*
Nastaveni terminalu na canon/no-canon mode
  stty -F /dev/tty icanon
  stty -F /dev/tty -icanon
  
Implementujte funkci read_line
  int read_line( char *buf, int len, int timeout_ms );
  
3. O_NONBLOCK (usleep( 10000);)
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/param.h>
#include <poll.h>

int read_line(char *buffer, int len, int timeout_s)
{
    int counter = 0;
    int flag = fcntl(STDIN_FILENO, F_GETFL);
    fcntl(STDIN_FILENO, F_SETFL, flag | O_NONBLOCK);

    struct timeval t_end, t_now, t_diff;
    gettimeofday(&t_end, NULL);
    t_end.tv_sec += timeout_s;

    while (1)
    {
        gettimeofday(&t_now, NULL);
        timersub(&t_end, &t_now, &t_diff);
        if (t_diff.tv_sec < 0)
        {
            printf("\nTimeout\n");
            return 0;
        }

        char ch;
        if (read(STDIN_FILENO, &ch, 1) > 0)
        {
            // printf("%c.", ch);

            if (ch == 10 || counter >= len)
                return 0;

            buffer[counter] = ch;
            counter++;
        }
        usleep(10000);
    }
}

int main()
{
    char buff[255] = {0};
    read_line(buff, sizeof(buff), 3);
    printf("Inserted text: %s\n", buff);
}