#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <sys/param.h>
#include <aio.h>

int handle = 0;
char buff[512];
aiocb io_control;

void signal(int sig)
{
    printf("handler");
    write(1, buff, io_control.aio_nbytes);

    io_control.aio_nbytes =sizeof(buff);
    aio_read(&io_control);
    // sigevent redirect to thread
}


int main()
{
    struct sigaction sa;
	sa.sa_handler = signal;
	sa.sa_flags = 0;
	sigemptyset(&sa.sa_mask);
	sigaction(SIGIO, &sa, NULL);;

    sigaction(SIGIO, &sa, NULL);

    io_control.aio_fildes = handle;
    io_control.aio_offset = 0;
    io_control.aio_buf = buff;
    io_control.aio_nbytes = sizeof(buff);
    io_control.aio_reqprio = 0;
    io_control.aio_sigevent.sigev_notify=SIGEV_SIGNAL;
    io_control.aio_sigevent.sigev_signo = SIGIO;

    return 0;
}