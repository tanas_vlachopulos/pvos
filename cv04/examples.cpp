#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <sys/param.h>

int handle = 0;

void signal_handler(int sig, siginfo_t *info, void *wtf)
{
	printf("get signal %d\n", sig);

	char buff[255] = {0};
	int r = read(handle, buff, sizeof(buff));
	// man sigaction - misto globalni promene handle pouzit rozsireny handler se siginfo, do flagu se musi pridat SA_SIGINFO tim se prepina handler z primitivniho na pokrocili
}

int main()
{
	struct sigaction sa;
	sa.sa_handler = signal_handler;
	sa.sa_flags = SA_SIGINFO;
	sigemptyset(&sa.sa_mask);
	sigaction(SIGIO, &sa, NULL);

	fcntl(handle, F_SETFL, fcntl(handle, F_GETFL) | O_ASYNC);
	fcntl(handle, F_SETOWN, getpid()); // musi byt jinak prestane po prvnim spusteni chodit sgnal SIGIO

	while(1)
	{
		printf("Nothink ...\n");
		sleep(1);
	}

	return 0;
}