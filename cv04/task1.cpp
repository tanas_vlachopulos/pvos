
/*
1. Pri komunikaci s pouzitim signalu SIGIO vyuzijte obsluhu signalu rozsirenou o siginfo_t.
Overte, kdyz budou dva potomci pomalu posilat rodici data pres dve roury, 
ze lze rozlisit zdroj (file descriptor) dat.

2. Pri komunikaci pres aio overte prevzeti dat pomoci vlakna (man sigevent / SIGEV_THREAD)

3. Overte, zda je omezen pocet vlaken, ktera se mohou vytvorit (viz prvni cviceni). 
Potomek pozila data rodici po 1 bajtu, napr. po 1 ms. (Overte vlakna pomoci pthread_self).
 
4. Vyzkousejte, zda je mozno predat aiocb do obsluhy signalu pres siginfo_t 
a polozku sigval(_t).
Overte s vice potomky a vice rourami, min 2. vla0054@linedu:/home/fei/ol
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <sys/param.h>
#include <aio.h>

#define LEN 2

void signal_handler(int sig, siginfo_t *info, void *wtf)
{
	printf("get signal %d, from file desc %d\n", sig, info->si_fd);

	char buff[128] = {0};
	int r = read(info->si_fd, buff, sizeof(buff));
	// man sigaction - misto globalni promene handle pouzit rozsireny handler se siginfo, do flagu se musi pridat SA_SIGINFO tim se prepina handler z primitivniho na pokrocili
}

int main()
{
	int pipes[LEN][2];
	for (int i = 0; i < LEN; i++)
	{
		pipe(pipes[i]);
	}

	for (int i = 0; i < 2; i++)
	{
		int pid = -1;
		pid = fork();

		// childs
		if (pid == 0)
		{
			printf("Create child %d\n", i);

			while (1)
			{
				char buf[128];
				sprintf(buf, "%d\n", getpid());
				printf("writing to pipe in child %d\n", i);
				write(pipes[i][1], buf, strlen(buf));
				sleep(1);
			}
		}
	}

	printf("Handle signal in parent");
	struct sigaction sa;
	sa.sa_sigaction = signal_handler;
	sa.sa_flags = SA_SIGINFO;
	sigemptyset(&sa.sa_mask);
	sigaction(SIGIO, &sa, NULL);

	for (int i = 0; i < LEN; i++)
	{
		fcntl(pipes[i][0], F_SETFL, fcntl(pipes[i][0], F_GETFL) | O_ASYNC);
		fcntl(pipes[i][0], F_SETOWN, getpid()); // musi byt jinak prestane po prvnim spusteni chodit sgnal SIGIO
		fcntl(pipes[i][0], F_SETSIG, SIGIO);
	}

	while (1)
	{
		printf("Nothing ...\n");
		sleep(1);
	}

	return 0;
}