// cviceni 4

// compile with  -lrt library !!!
// c++ task2.cpp -o task2.exe -lrt
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <fcntl.h>
#include <aio.h>
#include <pthread.h>

int handle = 0;

char buf[512];
aiocb io_control;

void t(union sigval sigval)
{
    printf("timer_nr=%02d  pid=%d  pthread_self=%ld\n",
           sigval.sival_int, getpid(), pthread_self());
    write(1, buf, io_control.aio_nbytes);
    io_control.aio_nbytes = sizeof(buf);
    aio_read(&io_control);
}

// task 2
int main()
{

    io_control.aio_fildes = handle;
    io_control.aio_offset = 0;
    io_control.aio_buf = buf;
    io_control.aio_nbytes = sizeof(buf);
    io_control.aio_reqprio = 0;
    io_control.aio_sigevent.sigev_notify = SIGEV_THREAD;
    io_control.aio_sigevent.sigev_notify_function = t;
    io_control.aio_sigevent.sigev_signo = SIGIO;

    aio_read(&io_control);

    while (1)
    {

        printf("nuda\n");
        sleep(1);
    }
}