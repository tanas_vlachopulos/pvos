/*
3. Overte, zda je omezen pocet vlaken, ktera se mohou vytvorit (viz prvni cviceni). 
  Potomek pozila data rodici po 1 bajtu, napr. po 1 ms. (Overte vlakna pomoci pthread_self).
*/

// compile with  -lrt library !!!
// c++ task3.cpp -o task3.exe -lrt
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <fcntl.h>
#include <aio.h>
#include <pthread.h>

int handle[2];
char buff[255];
aiocb io_control;

void t(union sigval val)
{
    printf("timer_nr=%02d  pid=%d  pthread_self=%ld\n", val.sival_int, getpid(), pthread_self());
}

// task 2
int main()
{
    pipe(handle);

    io_control.aio_fildes = handle[0];
    io_control.aio_offset = 0;
    io_control.aio_buf = buff;
    io_control.aio_nbytes = sizeof(buff);
    io_control.aio_reqprio = 0;

    io_control.aio_sigevent.sigev_notify = SIGEV_THREAD;
    io_control.aio_sigevent.sigev_notify_function = t;
    io_control.aio_sigevent.sigev_notify_attributes = NULL;
    io_control.aio_sigevent.sigev_signo = SIGIO;
    io_control.aio_sigevent.sigev_value.sival_ptr = &io_control;
    io_control.aio_lio_opcode = 0;

    int pid = -1;
    pid = fork();

    if (pid == 0) // child
    {
        while(1)
        {
            printf("-");

            sprintf(buff, "B");

            aio_write(&io_control);
            usleep(1000);
        }
    }
    else if (pid > 0)
    {
        aio_read(&io_control);
        
        while (1)
        {
            printf(".");
            sleep(1);
        }
    }
    
    
}