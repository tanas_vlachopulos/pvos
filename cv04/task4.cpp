/*
4. Vyzkousejte, zda je mozno predat aiocb do obsluhy signalu pres siginfo_t 
a polozku sigval(_t).
Overte s vice potomky a vice rourami, min 2. 
*/
// compile with  -lrt library !!!
// c++ task4.cpp -o task4.exe -lrt
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <sys/param.h>
#include <aio.h>

#define LEN 2
#define DATA_LEN 255

void signal_handler(int sig, siginfo_t *info, void *wtf)
{
	printf("get signal %d, from file desc %d\n", sig, info->si_fd);
	printf("obtained value from aiocb: %d\n", info->si_value);

	char buff[128] = {0};
	int r = read(info->si_fd, buff, sizeof(buff));
	// man sigaction - misto globalni promene handle pouzit rozsireny handler se siginfo, do flagu se musi pridat SA_SIGINFO tim se prepina handler z primitivniho na pokrocili
}

int main()
{
	// init pipes
	int pipes[LEN][2];
	for (int i = 0; i < LEN; i++)
	{
		pipe(pipes[i]);
	}

	//  ini aio controls
	aiocb aio_control[LEN];
	for (int i = 0; i < LEN; i++)
	{
		char buff[DATA_LEN];

		aio_control[i].aio_fildes = pipes[i][0];
		aio_control[i].aio_offset = 0;
		aio_control[i].aio_reqprio = 0;
		aio_control[i].aio_buf = buff;
		aio_control[i].aio_nbytes = DATA_LEN;
		aio_control[i].aio_sigevent.sigev_notify = SIGEV_SIGNAL;
		aio_control[i].aio_sigevent.sigev_signo = SIGIO;
		aio_control[i].aio_sigevent.sigev_notify_attributes = NULL;
		aio_control[i].aio_sigevent.sigev_value.sival_ptr = &aio_control[i];
		aio_control[i].aio_lio_opcode = 0;

		aio_read(&aio_control[i]);
	}

	// create data senders
	for (int i = 0; i < LEN; i++)
	{
		int pid = -1;
		pid = fork();

		// childs
		if (pid == 0)
		{
			printf("Create child %d\n", i);

			while (1)
			{
				char buf[255];
				// char *buff;
				// buff = (char*)aio_control[i].aio_buf;
				sprintf(buf, "%d\n", getpid());
				printf("writing to pipe in child %d\n", i);
				write(pipes[i][1], buf, strlen(buf));
				// aio_write(&aio_control[i]);
				sleep(1);
			}
		}
	}

	// handle data from childs
	printf("Handle signal in parent");
	struct sigaction sa;
	sa.sa_sigaction = signal_handler;
	sa.sa_flags = SA_SIGINFO;
	sigemptyset(&sa.sa_mask);
	sigaction(SIGIO, &sa, NULL);

	
	while (1)
	{
		for (int i = 0; i < LEN; i++)
		{
			aio_read(&aio_control[i]);
		}
		printf("Nothing ...\n");
		sleep(1);
	}

	return 0;
}