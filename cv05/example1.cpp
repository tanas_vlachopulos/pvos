#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <sys/param.h>

// cat > roura  takto muzeme presmeruje vystup do roury z konzole
int main()
{
    int roura = open("/tmp/trubka", O_RDONLY | O_NONBLOCK);
    printf("Trubka %d otevrena\n", roura);
    fcntl(roura, F_SETFL, fcntl(roura, F_GETFL) & ~O_NONBLOCK);
    while (1)
    {
        char buff[1024];
        int ret = read(roura, buff, sizeof(buff));
        if (ret > 0)
        {
            write(1, buff, ret);
        }
        else if (ret < 0)
        {
            printf("read selhal %d\n", ret);
            break;
        }
        else
        {
            printf("roura nema vstup\n");
            usleep(10000);
        }
    }
    close(roura);
}