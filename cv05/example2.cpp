#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <sys/param.h>
#include <sys/mman.h>
#include <sys/ipc.h>
#include <sys/shm.h>

// soubor namapovany ve sdilene pameti
// man ftruncate - nafukovani souboru na pozadovanou delku
/*
int main()
{
    int data = open("data.dat", O_CREAT | O_RDWR, 0660);
    ftruncate(data, 100);
    char *ptrdata = (char *) mmap(NULL, 100, PROT_READ | PROT_WRITE, MAP_SHARED, data, 0);
    strcpy(ptrdata, "Blahoslaveni budiz oraj\nBezverci zhori v ohni vecneho zatraceni");
    msync(ptrdata, 100, MS_SYNC);
    
    close(data);
}
*/

// vytvori sdileny pametovy prostor kt neni odvozen ze souboru
/*
int main()
{
    char *ptrdata = (char *) mmap(NULL, 100, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0); // data musi by nastavena na -1 jinak funguje nekorektne
    *ptrdata=0;
    if (fork() == 0)
    {
        sleep(1);
        printf("potomek vidi %s \n", ptrdata);
        exit(0);
    }
    
    strcpy(ptrdata, "Blahoslaveni budiz oraj\nBezverci zhori v ohni vecneho zatraceni\n");
    msync(ptrdata, 100, MS_SYNC);
    
    wait(NULL);
}
*/

// hexdump
// system V
int main()
{
    int memid = shmget(0xbeefbeef, 100, IPC_CREAT, 0660); // sdilena pamet neni vazana k souboru ale k tomuto ID
    char *ptrdata = (char *) shmat(memid, NULL, 0);
    *ptrdata = 0;
    
    if (fork() == 0)
    {
        sleep(1);
        printf("potomek vidi %s \n", ptrdata);
        exit(0);
    }
    
    strcpy(ptrdata, "Blahoslaveni budiz oraj\nBezverci zhori v ohni vecneho zatraceni\n");
    
    wait(NULL);

}