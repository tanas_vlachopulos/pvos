/*
1. Zmerte rychlost preneseni 100GB data mezi procesy pomoci anonymni roury, pojmenovane roury a
  pomoci sdilene pameti, kdy sdilena pamet bude cca 1-10MB a 2 roury budou synchronizovat data. 
  Prenaset se budou inty, obe strany udelaji soucet do long.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <sys/param.h>
#include <sys/mman.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define DATASIZE 2500000000
// #define DATASIZE 10000000000
#define BUFFSIZE 16000
// #define SHAREDM 10000000
#define SHAREDM 10000000

void named_pipe()
{

	int pid = -1;
	pid = fork();
	if (pid == 0) // child
	{
		// reading from named pipe
		int tube_read = open("/tmp/roura", O_RDONLY | O_NONBLOCK);
		printf("pipe %d open for read\n", tube_read);
		fcntl(tube_read, F_SETFL, fcntl(tube_read, F_GETFL) & ~O_NONBLOCK);

		int read_buffer[BUFFSIZE] = {0};
		long int final_sum = 0;
		// usleep(100);

		while (1)
		{
			int ret = read(tube_read, read_buffer, BUFFSIZE);
			// printf("read %d bytes from pipe\n", ret);
			if (ret <= 0)
			{
				printf("read from pipe failed\n");
				printf("final sum in reader %ld\n", final_sum);
				exit(0);
			}
			else
			{
				for (long i = 0; i < BUFFSIZE; i++)
				{
					int index_value = read_buffer[i];
					// printf("value on %d = %d\n", i, index_value);
					final_sum += index_value;
				}
				// printf("<- %ld\n", final_sum);
			}
			memset(read_buffer, 0, sizeof(read_buffer));
		}
		wait(NULL);
	}
	else if (pid > 0) // parent
	{
		srand(getpid());
		// writing to named pipe
		int buffer[BUFFSIZE] = {0};
		long int final_sum = 0;

		int tube = open("/tmp/roura", O_WRONLY);
		printf("pipe %d open for write\n", tube);

		for (long i = 0; i < DATASIZE; i += BUFFSIZE)
		{
			for (long ii = 0; ii < BUFFSIZE; ii++)
			{
				int add_num = rand() % 10;
				buffer[ii] = add_num;
				final_sum += add_num;
			}
			// printf("-> %ld\n", final_sum);
			write(tube, buffer, BUFFSIZE * sizeof(int));
			// usleep(100);
		}

		printf("final sum in writer: %ld\n", final_sum);
	}
}

void anon_pipe()
{
	int tube[2];
	pipe(tube);

	int pid = -1;
	pid = fork();
	if (pid == 0) // child
	{
		int read_buffer[BUFFSIZE] = {0};
		long int final_sum = 0;
		// usleep(100);

		printf("Start read from pipe in child\n");
		while (1)
		{
			int ret = read(tube[0], read_buffer, BUFFSIZE);
			// printf("read %d bytes from pipe\n", ret);
			if (ret <= 0)
			{
				printf("read from anon pipe failed\n");
				printf("final sum in reader %ld\n", final_sum);
				exit(0);
			}
			else
			{
				for (long i = 0; i < BUFFSIZE; i++)
				{
					int index_value = read_buffer[i];
					if (index_value == 666)
					{
						printf("read from anon pipe failed\n");
						printf("final sum in reader %ld\n", final_sum);
						exit(0);
					}

					final_sum += index_value;
				}
				// printf("<- %ld\n", final_sum);
			}
			memset(read_buffer, 0, sizeof(read_buffer));
		}
	}
	else if (pid > 0) // parent
	{
		srand(getpid());
		// writing to named pipe
		int buffer[BUFFSIZE] = {0};
		long int final_sum = 0;

		// int tube = open("/tmp/roura", O_WRONLY);
		// printf("pipe %d open for write\n", tube);
		printf("Start write into anon pipe in parent\n");

		for (long i = 0; i < DATASIZE; i += BUFFSIZE)
		{
			for (long ii = 0; ii < BUFFSIZE; ii++)
			{
				int add_num = rand() % 10;
				buffer[ii] = add_num;
				final_sum += add_num;
			}
			// printf("-> %ld\n", final_sum);
			write(tube[1], buffer, BUFFSIZE * sizeof(int));
			// usleep(100);
		}

		buffer[0] = 666;
		write(tube[1], buffer, BUFFSIZE * sizeof(int));

		printf("final sum in writer: %ld\n", final_sum);
		wait(NULL);
	}
}

void shared_mem()
{
	int tube[2];
	pipe(tube);

	int *data = (int *)mmap(NULL, SHAREDM, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
	*data = 0;

	int pid = -1;
	pid = fork();
	if (pid == 0) // child
	{
		char ind;
		long reader_sum = 0;

		while (1)
		{
			read(tube[0], &ind, 1);
			// printf("ind %d ", ind);
			if (ind == 1)
			{
				for (long i = 0; i < (SHAREDM / sizeof(int)); i++)
				{
					// printf("%d ", data[i]);
					reader_sum += data[i];
					// if (data[i] != 1)
					//     printf("bad values\n");
				}
				// printf("\n---------------\n");
				// sleep(1);
			}
			else if (ind == 2)
			{
				printf("reader data sum: %ld\n", reader_sum);
				exit(0);
			}
			
		}
	}
	else if (pid > 0) // parent
	{
		srand(getpid());
		long int writer_sum = 0;
		char ind = 1;

		for (long ii = 0; ii < DATASIZE; ii += SHAREDM )
		{
			memset(data, 0, SHAREDM);
			for (long i = 0; i < (SHAREDM / sizeof(int)); i++)
			{
				int value = rand() % 10;
				data[i] = value;
				writer_sum += value;
			}
			write(tube[1], &ind, 1);
			usleep(250000);
		}
		ind = 2;
		write(tube[1], &ind, 1);
		printf("writer data sum: %ld\n", writer_sum);
		wait(NULL);
	}
}

int main()
{
	// named_pipe(); // 40s
	// anon_pipe(); // 40s
	shared_mem(); // 1m14s

}
