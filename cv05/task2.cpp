/*2. Overte, ze data v souboru je mozne pouzit stejne pres read/write i pres mmap.
	struct data { float cisla[ 10 ], suma };
	
	Rodic vytvari postupne potomky, ne soucasne vice. Potomek nahodne precte soubor pres read, 
	nebo pres mmap, zkontroluje soucet, zmeni jedno cislo, upravi soucet a ulozi data. 
	*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <sys/param.h>
#include <sys/mman.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define LOOPS 10

struct data
{
	float numbers[10];
	float suma;
};

bool count_data(data *d)
{
	int sum = 0;
	for (int i = 0; i < 10; i++)
	{
		printf("%f ", d->numbers[i]);
		sum += d->numbers[i];
	}
	printf(" = %f\n", d->suma);
	return (sum == d->suma) ? true : false;
}

void rewrite_data(data *ptr)
{
	if (count_data(ptr))
	{
		printf("Presum OK\n");
	}
	else
	{
		printf("Presum NOT OK\n");
	}

	int index = rand() % 10;
	float new_value = static_cast<float>(rand()) / (static_cast<float>(RAND_MAX / 30));
	float old_value = ptr->numbers[index];
	ptr->numbers[index] = new_value;
	ptr->suma += new_value - old_value;

	if (count_data(ptr))
	{
		printf("Postsum OK\n");
	}
	else
	{
		printf("Postsum NOT OK\n");
	}
}

int main()
{
	data chunk = {0};
	memset(chunk.numbers, 0, 10*sizeof(float));
	chunk.suma=0;

	int fd = open("/tmp/data1.dat", O_CREAT | O_WRONLY, 0660);
	write(fd, &chunk, sizeof(data));
	close(fd);

	for (int i = 0; i < LOOPS; i++)
	{
		// printf("Loop %d\n", i);

		int pid = fork();
		if (pid == 0) // child
		{
			int file = open("/tmp/data1.dat", O_CREAT | O_RDWR, 0660);
			ftruncate(file, sizeof(data));

			srand(getpid());
			if (rand() % 2)
			{
				printf ("\nMmap child ...\n");
				data *ptr = (data *)mmap(NULL, sizeof(data), PROT_READ | PROT_WRITE, MAP_SHARED, file, 0);
				
				printf("rewriting\n");
				rewrite_data(ptr);

				msync(ptr, sizeof(data), MS_SYNC);
			}
			else
			{
				printf("\nRead/Write child ...\n");
				data *ptr = {0};
				read(file, ptr, sizeof(data));
				
				printf("rewriting\n");				
				rewrite_data(ptr);

				write(file, ptr, sizeof(data));
			}

			close(file);
			exit(0);
		}

		sleep(1);
		waitpid(pid, NULL, 0);
	}
}