#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <sys/param.h>
#include <sys/mman.h>
#include <sys/ipc.h>
#include <sys/sem.h>

// system V semafor
int main()
{

    int semid = semget(0xDEADBEEF, 1, IPC_CREAT | 0600);
    semctl(semid, 0, SETVAL, 1);
    sembuf up = {0, 1, 0}; // pokud tady budou same 0 tak bude semop cekat tak dlouho dokud se neobjevi v semaforu 0
    sembuf down = {0, -1, 0};

    printf("semid = %d\n", semid);
    
    int pid = fork();
    while(1)
    {
        int a, b;
        printf("%d: enter two numbers: \n", getpid());
        semop(semid, &down, 1);
        scanf("%d", &a);
        scanf("%d", &b);
        semop(semid, &up, 1);
        printf("%d: this is your numbers %d, %d\n", getpid(), a, b);
    }
    
    exit(0);
}