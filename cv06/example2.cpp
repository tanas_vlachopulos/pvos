#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <sys/param.h>
#include <sys/mman.h>
#include <sys/ipc.h>
#include <semaphore.h>

// posixovy semafor
int main()
{
    sem_t *semafor = sem_open("/semaforek", O_CREAT | O_RDWR, 0660, 1); // je jedno jestli si vytvorim sdilenou pamet a pak tam umistim semafor, nebo to otevru takhle
    sem_init(semafor, 1, 1);                                          // anonymni sdileny semafor, pokud to ma fungovat mezi procesy tak musi byt druhy parametr 1 tzn je sdileny
    // man sem_init

    while (1)
    {
        int a, b;
        printf("%d: enter two numbers: \n", getpid());
        sem_wait(semafor);
        scanf("%d", &a);
        scanf("%d", &b);
        sem_post(semafor);
        printf("%d: this is your numbers %d, %d\n", getpid(), a, b);
    }

    exit(0);
}