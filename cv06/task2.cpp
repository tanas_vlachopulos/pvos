/*

!!! compile like this: c++  task1.cpp -o task1.exe -std=c++11
*/
#include <iostream>
#include <unordered_map>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/ipc.h>
#include <sys/sem.h>

#define MAX_LOAD 30
#define CHILDS 10

struct box
{
    int count;
    int items[MAX_LOAD];
};

void print_stats(std::unordered_map<int, int> data)
{
    // std::vector<int> keys;
    // keys.reserve(data.size());
    printf("==================================\n");
    for (auto key : data)
    {
        printf("Loader %d\tmade %d\t operations\n", key.first, key.second);
    }
    printf("==========================================\n");
}

int main()
{
    int sem_put_item = semget(0xcafecafe, 1, IPC_CREAT | 0600);
    semctl(sem_put_item, 0, SETVAL, 1);

    int sem_box = semget(0xbeefbeef, 1, IPC_CREAT | 0600);
    semctl(sem_box, 0, SETVAL, MAX_LOAD);

    sembuf up = {0, 1, 0}; // pokud tady budou same 0 tak bude semop cekat tak dlouho dokud se neobjevi v semaforu 0
    sembuf down = {0, -1, 0};
    sembuf empty = {0, 0, 0};

    box *sh_box = (box *)mmap(NULL, sizeof(box), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    sh_box->count = 0;
    memset(sh_box->items, 0, MAX_LOAD * sizeof(int));

    // create childs
    for (int i = 0; i < CHILDS; i++)
    {
        int pid = fork();
        if (pid == 0) // child
        {
            while (1)
            {
                semop(sem_put_item, &down, 1); // lock insert critical section
                // printf("Child %d put intem on index %d\n", getpid(), sh_box->count);
                sh_box->items[sh_box->count] = getpid();
                sh_box->count++;

                if (semctl(sem_box, 0, GETVAL, 0) > 1)
                {
                    semop(sem_box, &down, 1);    // remove free slot from box capacity
                    semop(sem_put_item, &up, 1); // unlock insert critical section
                }
                else
                {
                    semop(sem_box, &down, 1); // remove free slot from box capacity
                }
            }
        }
    }

    // parent
    std::unordered_map<int, int> data; // storage for statistics
    long counter = 0;

    while (1)
    {
        semop(sem_box, &empty, 1); // wait for 0 in semaphor
        semctl(sem_box, 0, SETVAL, MAX_LOAD);
        // printf("Emptying box\n");

        // count stats
        for (int i = 0; i < MAX_LOAD; i++)
        {
            data[sh_box->items[i]]++;
        }

        if ((counter % 20) == 0)
        {
            printf("=== LOOP %ld ", counter);
            print_stats(data);
            printf("Emptying box\n");
        }

        sh_box->count = 0;
        memset(sh_box->items, 0, MAX_LOAD * sizeof(int));

        usleep(40000);
        counter++;

        semop(sem_put_item, &up, 1); // unlock insert critical section
    }
}