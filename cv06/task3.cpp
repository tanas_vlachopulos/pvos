/*

!!! compile like this: c++  task1.cpp -o task1.exe -std=c++11 -lpthread
*/
#include <iostream>
#include <unordered_map>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <semaphore.h>
#include <fcntl.h>

#define MAX_LOAD 30
#define CHILDS 10

struct box
{
    int count;
    int items[MAX_LOAD];
};

void print_stats(std::unordered_map<int, int> data)
{
    // std::vector<int> keys;
    // keys.reserve(data.size());
    printf("==================================\n");
    for (auto key : data)
    {
        printf("Loader %d\tmade %d\t operations\n", key.first, key.second);
    }
    printf("==========================================\n");
}

int main()
{
    sem_t *sem_put_item = sem_open("/sem_put_item", O_CREAT | O_RDWR, 0660, 1);
    sem_init(sem_put_item, 1, 1);

    sem_t *sem_box = sem_open("/sem_box", O_CREAT | O_RDWR, 0660, 1);
    sem_init(sem_box, 1, 0);

    box *sh_box = (box *)mmap(NULL, sizeof(box), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    sh_box->count = 0;
    memset(sh_box->items, 0, MAX_LOAD * sizeof(int));

    // create childs
    for (int i = 0; i < CHILDS; i++)
    {
        int pid = fork();
        if (pid == 0) // child
        {
            while (1)
            {
                sem_wait(sem_put_item); // lock insert section
                // printf("Child %d put intem on index %d\n", getpid(), sh_box->count);
                sh_box->items[sh_box->count] = getpid();
                sh_box->count++;
                if (sh_box->count >= MAX_LOAD) // unlock box semafor if box is full
                {
                    sem_post(sem_box);
                    continue; // skip insertion unlock it is parent job now
                }
                sem_post(sem_put_item); // unlock insert critical section
                usleep(10);
            }
        }
    }

    // parent
    std::unordered_map<int, int> data; // storage for statistics
    long counter = 0;

    while (1)
    {
        sem_wait(sem_box); // wait for box semafor unlock
        // printf("Emptying box\n");

        // count stats
        for (int i = 0; i < MAX_LOAD; i++)
        {
            data[sh_box->items[i]]++;
        }

        if ((counter % 20) == 0)
        {
            // for (int i = 0; i < MAX_LOAD; i++)
            //     printf("%d ", sh_box->items[i]);
            printf("\n");
            printf("=== LOOP %ld ", counter);
            print_stats(data);
            printf("Emptying box\n");
        }

        sh_box->count = 0;
        memset(sh_box->items, 0, MAX_LOAD * sizeof(int));

        usleep(40000);
        counter++;

        sem_post(sem_put_item); // unlock insert section
    }
}