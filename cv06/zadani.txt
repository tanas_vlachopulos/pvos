Napiste aplikaci simulujici plneni prepravky pro MAX vyrobku. 
Rodic bude mit minimalne 10 potomku.
Potomci plni prepravku, ta je ve sdilene pameti: struct { int pocet, vyrobky[ MAX ]; }
Kazdy potomek vzdy precte pocet, zapise svuj pid mezi vyrobky[pocet++]
Pokud potomek vlozi na posledni volne misto, musi potomci pockat na vymenu prepravky.

1. Implementace pomoci semaforu System V, rodic snizuje semafor prepracky o MAX.
  Rodic pri vyprazdneni prepravky dela statistiku, kolik vyrobku dodal ktery potomek 
  (jeho pid ve sdilene pameti). Vhodne pouzit unordered_map (hashovaci tabulka), c++11. 

2. Implementace pomoci semaforu System V, rodic vyuzije vlastnosti wait-for-zero. 

3. Implementace pomoci Posix semaforu. 
