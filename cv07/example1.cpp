#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <sys/param.h>
#include <sys/mman.h>
#include <sys/ipc.h>
#include <sys/msg.h>

struct myMsg
{
    long type;
    union {
        int intnum;
        float floatnum;
    };
};

#define myMsg_size ((sizeof(myMsg) - sizeof(long)))

int main()
{
    int msg = msgget(0xbeefbeef, IPC_CREAT | 0600);
    printf("Msg id %d\n", msg);

    if (fork() == 0)
    {
        while (1)
        {
            srand(time(NULL));
            myMsg mm;
            mm.type = (rand() % 2) + 1;
            if (mm.type == 1)
            {
                int r = rand() % 100;
                printf("Sending int %d\n", r);
                mm.intnum = r;
            }
            else
            {
                float r = ((float)(rand() % 100000) / 100);
                printf("Sending float %f\n", r);
                mm.floatnum = r;
            }
            msgsnd(msg, &mm, myMsg_size, 0);
            usleep(400000);
        }
    }
    else
    {
        while (1)
        {
            myMsg mm;
            msgrcv(msg, &mm, myMsg_size, 0, 0);

            if (mm.type == 1)
            {
                printf("Int number %d\n", mm.intnum);
            }
            else
            {
                printf("float num %f\n", mm.floatnum);
            }
        }
    }
    exit(0);
}