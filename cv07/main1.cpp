#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/param.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/msg.h>

#define CELE 1
#define DESETINNE 2

struct mymsg
{
	long type;
	union
	{
		int celecislo;
		float descislo;
	};
};

#define mymsg_size	( ( sizeof( mymsg ) - sizeof( long ) ) )

int main()
{
	int msg = msgget( 0xcafecafe, IPC_CREAT | 0600 );
	printf( "msg_id %d\n", msg );

	if ( fork() == 0 )
	{ // potomek
		while ( 1 )
		{
			mymsg mm;
			mm.type = ( rand() % 2 ) + 1 ;
			switch ( mm.type )
			{
			case CELE:
				mm.celecislo = rand() % 1000;
				printf( "odesilam cele cislo %d\n", mm.celecislo );
			break;
			case DESETINNE:
				mm.descislo = ( ( float ) ( rand() % 100000 ) / 100 );
				printf( "odesilam desetinne cislo %f\n", mm.descislo );
			break;
			}
			msgsnd( msg, &mm, mymsg_size, 0 );
			usleep( 10000 );
		}
	}
	else
	{
		while ( 1 )
		{
			mymsg mm;
			msgrcv( msg, &mm, mymsg_size, 0, 0 );
			switch ( mm.type )
			{
			case CELE:
				printf( "prijato cele cislo %d\n", mm.celecislo );
			break;
			case DESETINNE:
				printf( "prijato desetinne cislo %f\n", mm.descislo );
			break;
			}

		}
	}

}
