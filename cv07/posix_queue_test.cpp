#include <fcntl.h>    /* For O_* constants */
#include <sys/stat.h> /* For mode constants */
#include <mqueue.h>
#include <stdio.h>

int main()
{
    struct mq_attr ma;
    ma.mq_flags = 0;
    ma.mq_maxmsg = 6;
    ma.mq_msgsize = sizeof(int);
    ma.mq_curmsgs = 0;
    mqd_t mq_insert = mq_open("/fronta_test", O_RDWR | O_CREAT, 0600, &ma);
    printf("%d\n", mq_insert);

    int msg = 1;
    mq_send(mq_insert, (char *)&msg, sizeof(int), 2);
    msg = 10;
    mq_send(mq_insert, (char *)&msg, sizeof(int), 3);
    
    int response;
    mq_receive(mq_insert, (char *)&response, sizeof(int), 0);
    printf("response %d\n", response);
}