#include <iostream>
#include <unordered_map>
#include <vector>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/param.h>
#include <sys/mman.h>
#include <sys/ipc.h>
#include <mqueue.h>


#define MAX_LOAD 30
#define CHILDS 10

#define queue_size ((sizeof(queue) - sizeof(long)))

// compile: c++ task1.2.cpp -o task1.2.exe -lrt -std=c++11  
struct box
{
    int count;
    int items[MAX_LOAD];
};

struct queue
{
    int freespace;
};

void print_stats(std::unordered_map<int, int> data)
{
    // std::vector<int> keys;
    // keys.reserve(data.size());
    printf("==================================\n");
    for (auto key : data)
    {
        printf("Loader %d\tmade %d\t operations\n", key.first, key.second);
    }
    printf("==========================================\n");
}

int main()
{
    // int msg = msgget(0xbeefbeef, IPC_CREAT | 0600);

    // queue insert_lock;
    // insert_lock.type = 1;
    // insert_lock.freespace = MAX_LOAD;
    // msgsnd(msg, &insert_lock, queue_size, 0);

    struct mq_attr ma;
    ma.mq_flags = 0;
    ma.mq_maxmsg = MAX_LOAD;
    ma.mq_msgsize = sizeof(int);
    ma.mq_curmsgs = 0;
    mqd_t mq_insert = mq_open("/fronta_insert2", O_RDWR | O_CREAT, 0600, &ma);
    printf("mq insert %d\n", mq_insert);
    
    mqd_t mq_box = mq_open("/fronta_box", O_RDWR | O_CREAT, 0600, &ma);
    printf("mq box %d\n", mq_box);

    if (mq_insert < 1 || mq_box < 1)
    {
        printf("cannot obtain semaphore\n");
        return 1;
    }
    
    int msg_insert = MAX_LOAD;
    mq_send(mq_insert, (char *)&msg_insert, sizeof(int), 0);

    box *sh_box = (box *)mmap(NULL, sizeof(box), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    sh_box->count = 0;
    memset(sh_box->items, 0, MAX_LOAD * sizeof(int));

    // create childs
    for (int i = 0; i < CHILDS; i++)
    {
        int pid = fork();
        if (pid == 0) // child
        {
            int box_lock;
                        
            while (1)
            {
                int insert_lock;
                mq_receive(mq_insert, (char *)&insert_lock, sizeof(int), 0);

                // printf("Insert lock contain %d freespace\n", insert_lock);
                // sleep(1);
                // printf("Child %d put intem on index %d\n", getpid(), sh_box->count);
                sh_box->items[sh_box->count] = getpid();
                sh_box->count++;
                insert_lock--;

                if (insert_lock > 0)
                {
                    // msgsnd(msg, &insert_lock, queue_size, 0); 
                    mq_send(mq_insert, (char *)&insert_lock, sizeof(int), 0);
                }
                else
                {
                    // msgsnd(msg, &box_lock, queue_size, 0); // box is full
                    mq_send(mq_box, (char *)&box_lock, sizeof(int), 0);
                }
                usleep(10);
            }
        }
    }

    // parent
    std::unordered_map<int, int> data; // storage for statistics
    long counter = 0;

    while (1)
    {
        int box_lock;
        mq_receive(mq_box, (char *)&box_lock, sizeof(int), 0);

        // count stats
        for (int i = 0; i < MAX_LOAD; i++)
        {
            data[sh_box->items[i]]++;
        }

        if ((counter % 20) == 0)
        {
            printf("=== LOOP %ld ", counter);
            print_stats(data);
            printf("Emptying box\n");
        }

        sh_box->count = 0;
        memset(sh_box->items, 0, MAX_LOAD * sizeof(int));

        usleep(40000);
        // sleep(4);
        counter++;

        int insert_lock = MAX_LOAD;
        // msgsnd(msg, &insert_lock, queue_size, 0);
        mq_send(mq_insert, (char *)&insert_lock, sizeof(int), 0);
    }
}