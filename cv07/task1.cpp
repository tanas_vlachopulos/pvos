#include <iostream>
#include <unordered_map>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/select.h>
#include <sys/param.h>
#include <sys/mman.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/msg.h>

#define MAX_LOAD 30
#define CHILDS 10

#define queue_size ((sizeof(queue) - sizeof(long)))

struct box
{
    int count;
    int items[MAX_LOAD];
};

struct queue
{
    long type;
    int freespace;
};

void print_stats(std::unordered_map<int, int> data)
{
    // std::vector<int> keys;
    // keys.reserve(data.size());
    printf("==================================\n");
    for (auto key : data)
    {
        printf("Loader %d\tmade %d\t operations\n", key.first, key.second);
    }
    printf("==========================================\n");
}

int
main()
{
    int msg = msgget(0xbeefbeef, IPC_CREAT | 0600);

    queue insert_lock;
    insert_lock.type = 1;
    insert_lock.freespace = MAX_LOAD;
    msgsnd(msg, &insert_lock, queue_size, 0);

    box *sh_box = (box *)mmap(NULL, sizeof(box), PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    sh_box->count = 0;
    memset(sh_box->items, 0, MAX_LOAD * sizeof(int));

    // create childs
    for (int i = 0; i < CHILDS; i++)
    {
        int pid = fork();
        if (pid == 0) // child
        {
            queue box_lock;
            box_lock.type = 2;
            box_lock.freespace = 0;

            queue insert_lock;

            while (1)
            {
                msgrcv(msg, &insert_lock, queue_size, 1, 0);

                // printf("Child %d put intem on index %d\n", getpid(), sh_box->count);
                sh_box->items[sh_box->count] = getpid();
                sh_box->count++;
                insert_lock.freespace--;

                if (insert_lock.freespace > 0)
                {
                    msgsnd(msg, &insert_lock, queue_size, 0); // remove free space from box
                }
                else
                {
                    msgsnd(msg, &box_lock, queue_size, 0); // box is full
                }
            }
        }
    }

    // parent
    std::unordered_map<int, int> data; // storage for statistics
    long counter = 0;

    while (1)
    {
        queue box_lock;
        msgrcv(msg, &box_lock, queue_size, 2, 0);

        // count stats
        for (int i = 0; i < MAX_LOAD; i++)
        {
            data[sh_box->items[i]]++;
        }

        if ((counter % 20) == 0)
        {
            printf("=== LOOP %ld ", counter);
            print_stats(data);
            printf("Emptying box\n");
        }

        sh_box->count = 0;
        memset(sh_box->items, 0, MAX_LOAD * sizeof(int));

        usleep(40000);
        counter++;

        queue insert_lock;
        insert_lock.type = 1;
        insert_lock.freespace = MAX_LOAD;
        msgsnd(msg, &insert_lock, queue_size, 0); 
    }
}