#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/ipc.h>
#include <sys/sem.h>

#define SIZE 10
#define SEMAPHORAS 8
#define CHILDS 1
#define MEM_SIZE ((sizeof(message) * 8 * SIZE))

struct message
{
    unsigned int type;
    char data[12];
};

sembuf up = {0, 1, 0};
sembuf down = {0, -1, 0};

// add item msg into queue data controled by sem
void push(message msg, int sem, message *data)
{
    sembuf lock = {0, -1, 0};
    sembuf unlock = {0, 1, 0};

    semop(sem, &lock, 1);
    up.sem_num = msg.type;

    printf("lock semaphor push %d\n", msg.type);
    int msg_count = semctl(sem, msg.type, GETVAL, 0);

    if (msg_count <= SIZE)
    {
        data[(msg.type - 1) * (SIZE) + msg_count] = msg;
        semop(sem, &up, 1);
        printf("Insert %s into queue %d\n", msg.data, msg.type);
        printf("Queue %d has %d free slots.\n", msg.type, SIZE - msg_count - 1);
    }
    else
    {
        printf("Queue %d is full.\n", msg.type);
    }

    semop(sem, &unlock, 1);
}

// get head item msg from queue data controled by sem
message pull(int type, int sem, message *data)
{
    sembuf lock = {0, -1, 0};
    sembuf unlock = {0, 1, 0};

    semop(sem, &lock, 1);

    down.sem_num = type;
    printf("pulling from queue with level %d\n", semctl(sem, down.sem_num, GETVAL, 0));
    semop(sem, &down, 1);

    int msg_count = semctl(sem, type, GETVAL, 0);
    message msg = data[((type - 1) * SIZE)];
    memmove(data + ((type - 1) * SIZE), data + ((type - 1) * SIZE) + 1, (msg_count + 1) * sizeof(message));
    printf("Remove %s from quue %d\n", msg.data, type);
    printf("Queue %d contain another %d messages.\n", type, msg_count);

    semop(sem, &unlock, 1);

    return msg;
}

// check if queue is empty
int is_empty(int type, int sem, message *data)
{
    sembuf lock = {0, -1, 0};
    sembuf unlock = {0, 1, 0};

    semop(sem, &lock, 1);
    int msg_count = semctl(sem, type, GETVAL, 0);
    semop(sem, &unlock, 1);

    return msg_count;
}

int main()
{
    printf("start\n");

    int semaphor = semget(0xcafecafe, SEMAPHORAS + 1, IPC_CREAT | 0600);
    for (int i = 1; i < SEMAPHORAS + 1; i++)
    {
        semctl(semaphor, i, SETVAL, 0);
    }
    semctl(semaphor, 0, SETVAL, 1);

    message *shdata = (message *)mmap(NULL, MEM_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    memset(shdata, 0, MEM_SIZE);

    for (int i = 0; i < 1; i++)
    {
        if (fork() == 0)
        {
            srand(getpid());
            while (1)
            {
                if (rand() % 2)
                {
                    message msg;
                    msg.type = random() % SEMAPHORAS + 1;
                    sprintf(msg.data, "pid %d", getpid());
                    printf("Pushing into %d\n", msg.type);

                    push(msg, semaphor, shdata);
                }
                else
                {
                    int random_type = random() % SEMAPHORAS + 1;
                    if (is_empty(random_type, semaphor, shdata) > 0)
                    {
                        printf("Pulling from %d\n", random_type);
                        message rcvmsg = pull(random_type, semaphor, shdata);
                    }
                    else
                    {
                        printf("Queue %d is empty.\n", random_type);
                    }
                }
                usleep(500000);
            }
        }
    }

    while (1)
    {
        sleep(1);
    }
}