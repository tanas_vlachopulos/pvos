#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>


int main( int argn, char **arg )
{
	addrinfo *adrres, adrreq;
	bzero( &adrreq, sizeof( adrreq ) );
	adrreq.ai_family = AF_INET;
	adrreq.ai_protocol = IPPROTO_TCP;

	getaddrinfo( arg[ 1 ], arg[ 2 ], &adrreq, &adrres );

	for ( addrinfo *a = adrres; a; a=a->ai_next )
	{
		printf( "%d %d %d %d\n", a->ai_family, a->ai_socktype, a->ai_protocol,
				ntohs( (( sockaddr_in * ) (a->ai_addr ) )->sin_port ) );
	}

	if ( !adrres )
	{
		printf( "server not found\n" );
		return 0;
	}

	int sd = socket( AF_INET, SOCK_STREAM, 0 );

	int err = connect( sd, adrres->ai_addr, adrres->ai_addrlen );
	if ( err < 0 ) printf( "connect selhal\n" );
	const char *pozdrav = "nazdar pres soket!";
	write( sd, pozdrav, strlen( pozdrav ) );
	close( sd );
}
