#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>


int main()
{
	sockaddr_in inadr;
	inadr.sin_family = AF_INET;
	inadr.sin_port = htons( 12345 );
	inadr.sin_addr.s_addr = INADDR_ANY;

	int sd = socket( AF_INET, SOCK_STREAM, 0 );
	int err = bind( sd, ( sockaddr * ) &inadr, sizeof( inadr ) );
	if ( err < 0 ) printf( "bind selhal\n" );
	err = listen( sd, 1 );

	sockaddr_in peeradr;
	socklen_t len_adr = sizeof( peeradr );
	int newsock = accept( sd, ( sockaddr * ) &peeradr, &len_adr );
	char data[ 256 ];
	err = read( newsock, data, sizeof( data ) );
	if ( err > 0 )
		write( 1, data, err );
	else
		printf( "read selhal\n" );

	close( newsock );
	close( sd );
}
