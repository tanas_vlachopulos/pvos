#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/un.h>
#include <sys/socket.h>
#include <sys/type.h>
#include <netinet/in.h>

int main()
{
    sockaddr_un inadr;
    inadr.sin_family = AF_INET;
    inadr.sin_port = htons(12345); // tato funkce prevadi nami zadane cislo do formatu v jakem sitover rozrahnni prima cisto portu
    inadr.sin_addr.s_addr = INADDR_ANY; // nasloucha na vsech adresach daneho pc

    int sd = socket(AF_UNIX, SOCK_STREAM, 0);
    int err = bind(sd, (sockaddr *)inadr, sizeof(inadr));
    if (err < 0)
    {
        printf("bind selhal \n");
    }

    err = listen(sd, 1); // druhy parametr je irelevantni
    if (err < 0)
        ;
    {
        printf("cannot listen\n");
    }

    socketlen_t len_inadr = sizeof(inadr);
    int newsoc = accept(sd, (sockaddr *)&inadr, &len_inadr);
    char data[256];

    err = read(newsoc, data, sizeof(data));

    if (err > 0)
    {
        write(1, data, err);
    }
    else
    {
        printf("fail\n");
    }

    close(newsoc);
}