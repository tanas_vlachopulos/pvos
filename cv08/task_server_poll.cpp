//***************************************************************************
//
// Program example for labs in subject Operating Systems
//
// Petr Olivka, Dept. of Computer Science, petr.olivka@vsb.cz, 2017
//
// Example of socket server.
//
// This program is example of socket server and it allows to connect and serve
// the only one client.
// The mandatory argument of program is port number for listening.
//
//***************************************************************************

/// Modified server with multiclient processing using poll
/// run as: ./task_server_poll.exe <port_number>

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <stdarg.h>
#include <sys/socket.h>
#include <sys/param.h>
#include <sys/time.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <vector>
#include <poll.h>
#include <sys/ioctl.h>

#define STR_CLOSE "close"
#define STR_QUIT "quit"

//***************************************************************************
// log messages

#define LOG_ERROR 0 // errors
#define LOG_INFO 1  // information and notifications
#define LOG_DEBUG 2 // debug messages

// debug flag
int debug = LOG_INFO;

void log_msg(int log_level, const char *form, ...)
{
    const char *out_fmt[] = {
        "ERR: (%d-%s) %s\n",
        "INF: %s\n",
        "DEB: %s\n"};

    if (log_level && log_level > debug)
        return;

    char buf[1024];
    va_list arg;
    va_start(arg, form);
    vsprintf(buf, form, arg);
    va_end(arg);

    switch (log_level)
    {
    case LOG_INFO:
    case LOG_DEBUG:
        fprintf(stdout, out_fmt[log_level], buf);
        break;

    case LOG_ERROR:
        fprintf(stderr, out_fmt[log_level], errno, strerror(errno), buf);
        break;
    }
}

//***************************************************************************
// help

void help(int num, char **arg)
{
    if (num <= 1)
        return;

    if (!strcmp(arg[1], "-h"))
    {
        printf(
            "\n"
            "  Socket server example.\n"
            "\n"
            "  Use: %s [-h -d] port_number\n"
            "\n"
            "    -d  debug mode \n"
            "    -h  this help\n"
            "\n",
            arg[0]);

        exit(0);
    }

    if (!strcmp(arg[1], "-d"))
        debug = LOG_DEBUG;
}

//***************************************************************************

int main(int argn, char **arg)
{
    int server_len, client_len;
    if (argn <= 1)
        help(argn, arg);

    int port = 0;

    // parsing arguments
    for (int i = 1; i < argn; i++)
    {
        if (!strcmp(arg[i], "-d"))
            debug = LOG_DEBUG;

        if (!strcmp(arg[i], "-h"))
            help(argn, arg);

        if (*arg[i] != '-' && !port)
        {
            port = atoi(arg[i]);
        }
    }

    if (port <= 0)
    {
        log_msg(LOG_INFO, "Bad or missing port number %d!", port);
        help(argn, arg);
    }

    log_msg(LOG_INFO, "Server will listen on port: %d.", port);

    // socket creation
    int sock_listen = socket(AF_INET, SOCK_STREAM, 0);
    if (sock_listen == -1)
    {
        log_msg(LOG_ERROR, "Unable to create socket.");
        exit(1);
    }

    in_addr addr_any = {INADDR_ANY};
    sockaddr_in srv_addr;
    srv_addr.sin_family = AF_INET;
    srv_addr.sin_port = htons(port);
    srv_addr.sin_addr = addr_any;

    server_len = sizeof(srv_addr);

    // Enable the port number reusing
    int opt = 1;
    if (setsockopt(sock_listen, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) < 0)
        log_msg(LOG_ERROR, "Unable to set socket option!");

    // assign port number to socket
    if (bind(sock_listen, (const sockaddr *)&srv_addr, sizeof(srv_addr)) < 0)
    {
        log_msg(LOG_ERROR, "Bind failed!");
        close(sock_listen);
        exit(1);
    }

    // listenig on set port
    if (listen(sock_listen, 1) < 0)
    {
        log_msg(LOG_ERROR, "Unable to listen on given port!");
        close(sock_listen);
        exit(1);
    }

    log_msg(LOG_INFO, "Enter 'quit' to quit server.");

    struct pollfd poll_set[30]; // connected clients
    int fd_count = 0;

    memset(poll_set, 0, sizeof(poll_set));
    poll_set[0].fd = sock_listen;
    poll_set[0].events = POLLIN;
    poll_set[1].fd = STDIN_FILENO;
    poll_set[1].events = POLLIN;

    fd_count += 2;

    // server mainloop
    while (1)
    {
        int sock_client = -1;
        int socket_maximum = -1;
        while (1) // wait for new client
        {
            int fd_index = 0;
            socket_maximum = sock_listen;

            poll(poll_set, fd_count, 0);

            if (poll_set[fd_index].revents & POLLIN)
            {
                sockaddr_in rsa;
                int rsa_size = sizeof(rsa);

                // new connection
                sock_client = accept(sock_listen, (sockaddr *)&rsa, (socklen_t *)&rsa_size);
                if (sock_client == -1)
                {
                    log_msg(LOG_ERROR, "Unable to accept new client.");
                    close(sock_listen);
                    exit(1);
                }

                poll_set[fd_count].fd = sock_client;
                poll_set[fd_count].events = POLLIN;
                fd_count++;
                uint lsa = sizeof(srv_addr);
                // my IP
                getsockname(sock_client, (sockaddr *)&srv_addr, &lsa);
                log_msg(LOG_INFO, "My IP: '%s'  port: %d",
                        inet_ntoa(srv_addr.sin_addr), ntohs(srv_addr.sin_port));
                // client IP
                getpeername(sock_client, (sockaddr *)&srv_addr, &lsa);
                log_msg(LOG_INFO, "Client IP: '%s'  port: %d",
                        inet_ntoa(srv_addr.sin_addr), ntohs(srv_addr.sin_port));
            }
            if (poll_set[1].revents & POLLIN)
            {
                char buf[128];

                // local cmd input
                ssize_t ret_read = read(0, buf, sizeof(buf));
                for (fd_index = 2; fd_index < fd_count; fd_index++)
                {
                    write(poll_set[fd_index].fd, buf, ret_read);
                }

                if (!strncasecmp(buf, "quit", strlen(STR_QUIT)))
                {
                    for (fd_index = 2; fd_index < fd_count; fd_index++)
                    {
                        close(poll_set[fd_index].fd);
                        fd_count--;
                    }
                    close(poll_set[1].fd);
                    fd_count--;
                    close(poll_set[2].fd);
                    fd_count--;
                    log_msg(LOG_INFO, "Request to 'quit' entered");
                    exit(0);
                }
            }

            for (fd_index = 2; fd_index < fd_count; fd_index++)
            {
                if (poll_set[fd_index].revents & POLLIN)
                {
                    char buf[128];
                    ssize_t ret_read = read(poll_set[fd_index].fd, buf, sizeof(buf));
                    if (!ret_read)
                    {
                        log_msg(LOG_DEBUG, "Client closed socket!");
                        close(poll_set[fd_index].fd);
                        fd_count--;
                    }
                    else if (ret_read < 0)
                        log_msg(LOG_DEBUG, "Unable to read data from client.");
                    else
                        log_msg(LOG_DEBUG, "Read %d bytes from client.", ret_read);

                    // close connection from client
                    if (!strncasecmp(buf, "close", strlen(STR_CLOSE)))
                    {
                        log_msg(LOG_INFO, "Client sent 'close' request to close connection.");
                        close(poll_set[fd_index].fd);
                        fd_count--;
                        log_msg(LOG_INFO, "Connection closed. Waiting for new client.");
                        // continue;
                    }
                    else
                    {
                        for (int fd_index2 = 1; fd_index2 < fd_count; fd_index2++)
                        {
                            if (poll_set[fd_index2].fd != poll_set[fd_index].fd)
                            {
                                ret_read = write(poll_set[fd_index2].fd, buf, ret_read);
                            }
                        }
                        if (ret_read < 0)
                            log_msg(LOG_ERROR, "Unable to write data to stdout.");
                    }
                }
            }
        }

        return 0;
    }
}
