#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <poll.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/uio.h>

struct telegram
{
	char head;
	int len;
	int crc;
	char foot;
	char data[0];
};

// telegram *ptele = malloc( sizeof( telegram ) + kolik_chci_dat );
// iovec list[ 3 ] =
// 		{ ptele->head, `crc-head` },
//		{ ptele->data, kolik_chci_dat },
//		{ ptele->crc, `data-crc` } };



// poslani deskriptoru mezi procesy
// deskriptor je transformovan a cilovemu procesu je vytvoren odpovidajici zaznam
// v tabulce deskriptoru
int main()
{
	int pair[ 2 ];
	socketpair( AF_UNIX, SOCK_STREAM, 0, pair );

	if ( fork() != 0 )
	{ // rodic
		int roura[ 2 ];
		pipe( roura );

		msghdr zprava;
		zprava.msg_name = NULL;
		zprava.msg_namelen = 0;
		zprava.msg_flags = 0;
		int pid = getpid();
		printf( "PID rodice je %d a posila fd %d\n", pid, roura[ 1 ] );
		iovec io = { &pid, sizeof( int ) };
		zprava.msg_iov = &io;
		zprava.msg_iovlen = 1; // doporuceno nemit prazdne zpravy
		char data[ CMSG_SPACE( sizeof( int ) ) ];
		zprava.msg_control = data;
		zprava.msg_controllen = sizeof( data );
		cmsghdr *cmsg = CMSG_FIRSTHDR( &zprava );
		cmsg->cmsg_level = SOL_SOCKET;
		cmsg->cmsg_type = SCM_RIGHTS;
		cmsg->cmsg_len = CMSG_LEN( sizeof( int ) );
		//memcpy( CMSG_DATA( cmsg ), roura + 1, sizeof( int ) );
		* ( int * ) CMSG_DATA( cmsg ) = roura[ 1 ];
		sendmsg( pair[ 0 ], &zprava, 0 );

		char buf[ 128 ];
		int l = read( roura[ 0 ], buf, sizeof( buf ) );
		if ( l > 0 )
			write( 1, buf, l );

		wait( NULL );
		return 0;
	}
	else
	{
		msghdr zprava;
		zprava.msg_name = NULL;
		zprava.msg_namelen = 0;
		zprava.msg_flags = 0;
		int pid;
		iovec io = { &pid, sizeof( int ) };
		zprava.msg_iov = &io;
		zprava.msg_iovlen = 1; // doporuceno nemit prazdne zpravy
		char data[ CMSG_SPACE( sizeof( int ) ) ];
		zprava.msg_control = data;
		zprava.msg_controllen = sizeof( data );

		pollfd pfd = { pair[ 1 ], POLLIN, 0 };
		poll( &pfd, 1, -1 );
		printf( "jsem za poll \n" );

		recvmsg( pair[ 1 ], &zprava, 0 );

		cmsghdr *cmsg = CMSG_FIRSTHDR( &zprava );
		int fd = * ( int * ) CMSG_DATA( cmsg );

		printf( "proces %d nam poslal fd %d\n", pid, fd );

		const char *pozdrav = "Ahojte pres poslany deskriptor!\n";
		write( fd, pozdrav, strlen( pozdrav ) );
		return 0;
	}
}

// ukazka writev
int main2()
{
	struct iovec list[ 3 ] = { { ( void * ) "Ahoj ", 0 }, {  ( void * ) "Karle", 0 }, {  ( void * ) "!\n", 0 } };
	for ( int i = 0; i < 3; i++ )
		list[ i ].iov_len = strlen( (char * ) list[ i ].iov_base );
	writev( 1, list, 3 );
	list[ 1 ].iov_base = (void*) "Pavle ";
	writev( 1, list, 3 );
}


int mmain()
{
	if ( fork() == 0 ) // potomek
	{
		sockaddr_un unadr;
		unadr.sun_family = AF_UNIX;
		strcpy( unadr.sun_path, "/tmp/nas_soket" );

		int sd = socket( AF_UNIX, SOCK_STREAM, 0 );
		int err = connect( sd, ( sockaddr * ) &unadr, sizeof( unadr ) );
		if ( err < 0 ) printf( "connect selhal\n" );
		const char *pozdrav = "nazdar pres soket!";
		write( sd, pozdrav, strlen( pozdrav ) );
		close( sd );
	}
	else // rodic
	{
		sockaddr_un unadr;
		unadr.sun_family = AF_UNIX;
		strcpy( unadr.sun_path, "/tmp/nas_soket" );

		unlink( unadr.sun_path );
		int sd = socket( AF_UNIX, SOCK_STREAM, 0 );
		int err = bind( sd, ( sockaddr * ) &unadr, sizeof( unadr ) );
		if ( err < 0 ) printf( "bind selhal\n" );
		err = listen( sd, 1 );

		socklen_t len_unadr = sizeof( unadr );
		int newsock = accept( sd, ( sockaddr * ) &unadr, &len_unadr );
		char data[ 256 ];
		err = read( newsock, data, sizeof( data ) );
		if ( err > 0 )
			write( 1, data, err );
		else
			printf( "read selhal\n" );

		close( newsock );
		close( sd );
	}
}
