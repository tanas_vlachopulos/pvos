//***************************************************************************
//
// Program example for labs in subject Operating Systems
//
// Petr Olivka, Dept. of Computer Science, petr.olivka@vsb.cz, 2017
//
// Example of socket server.
//
// This program is example of socket server and it allows to connect and serve
// the only one client.
// The mandatory argument of program is port number for listening.
//
//***************************************************************************

/// Modified server with multiclient processing using select
/// run as: ./task_server_select.exe <port_number>

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <stdarg.h>
#include <sys/socket.h>
#include <sys/param.h>
#include <sys/time.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <vector>

#include <openssl/rsa.h>       /* SSLeay stuff */
#include <openssl/crypto.h>
#include <openssl/x509.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/err.h>

#define STR_CLOSE "close"
#define STR_QUIT "quit"

/* define HOME to be dir for key and cert files... */
#define HOME "./"
/* Make these what you want for cert & key files */
#define CERTF  HOME "myserver.crt"
#define KEYF  HOME  "mypriv.pem"

//***************************************************************************
// log messages

#define LOG_ERROR 0 // errors
#define LOG_INFO 1  // information and notifications
#define LOG_DEBUG 2 // debug messages

// debug flag
int debug = LOG_INFO;

struct client
{
    int client_fd;
    SSL* ssl;
};

void log_msg(int log_level, const char *form, ...)
{
    const char *out_fmt[] = {
        "ERR: (%d-%s) %s\n",
        "INF: %s\n",
        "DEB: %s\n"};

    if (log_level && log_level > debug)
        return;

    char buf[1024];
    va_list arg;
    va_start(arg, form);
    vsprintf(buf, form, arg);
    va_end(arg);

    switch (log_level)
    {
    case LOG_INFO:
    case LOG_DEBUG:
        fprintf(stdout, out_fmt[log_level], buf);
        break;

    case LOG_ERROR:
        fprintf(stderr, out_fmt[log_level], errno, strerror(errno), buf);
        break;
    }
}

//***************************************************************************
// help

void help(int num, char **arg)
{
    if (num <= 1)
        return;

    if (!strcmp(arg[1], "-h"))
    {
        printf(
            "\n"
            "  Socket server example.\n"
            "\n"
            "  Use: %s [-h -d] port_number\n"
            "\n"
            "    -d  debug mode \n"
            "    -h  this help\n"
            "\n",
            arg[0]);

        exit(0);
    }

    if (!strcmp(arg[1], "-d"))
        debug = LOG_DEBUG;
}

//***************************************************************************

int main(int argn, char **arg)
{
    if (argn <= 1)
        help(argn, arg);

    int port = 0;

    // parsing arguments
    for (int i = 1; i < argn; i++)
    {
        if (!strcmp(arg[i], "-d"))
            debug = LOG_DEBUG;

        if (!strcmp(arg[i], "-h"))
            help(argn, arg);

        if (*arg[i] != '-' && !port)
        {
            port = atoi(arg[i]);
        }
    }

    if (port <= 0)
    {
        log_msg(LOG_INFO, "Bad or missing port number %d!", port);
        help(argn, arg);
    }

    log_msg(LOG_INFO, "Server will listen on port: %d.", port);

    // socket creation
    int sock_listen = socket(AF_INET, SOCK_STREAM, 0);
    if (sock_listen == -1)
    {
        log_msg(LOG_ERROR, "Unable to create socket.");
        exit(1);
    }

    in_addr addr_any = {INADDR_ANY};
    sockaddr_in srv_addr;
    srv_addr.sin_family = AF_INET;
    srv_addr.sin_port = htons(port);
    srv_addr.sin_addr = addr_any;

    // Enable the port number reusing
    int opt = 1;
    if (setsockopt(sock_listen, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) < 0)
        log_msg(LOG_ERROR, "Unable to set socket option!");

    // assign port number to socket
    if (bind(sock_listen, (const sockaddr *)&srv_addr, sizeof(srv_addr)) < 0)
    {
        log_msg(LOG_ERROR, "Bind failed!");
        close(sock_listen);
        exit(1);
    }

    // listenig on set port
    if (listen(sock_listen, 1) < 0)
    {
        log_msg(LOG_ERROR, "Unable to listen on given port!");
        close(sock_listen);
        exit(1);
    }

    log_msg(LOG_INFO, "Enter 'quit' to quit server.");

    int listen_sd;
    int sd;
    struct sockaddr_in sa_serv;
    struct sockaddr_in sa_cli;
    socklen_t client_len;
    SSL_CTX* ctx;
    X509*    client_cert;
    char*    str;
    const SSL_METHOD *meth;

    SSL_load_error_strings();
    SSLeay_add_ssl_algorithms();
    meth = SSLv23_server_method();
    ctx = SSL_CTX_new (meth);
    if (!ctx) {
        ERR_print_errors_fp(stderr);
        exit(2);
    }
    
    if (SSL_CTX_use_certificate_file(ctx, CERTF, SSL_FILETYPE_PEM) <= 0) {
        ERR_print_errors_fp(stderr);
        exit(3);
    }
    if (SSL_CTX_use_PrivateKey_file(ctx, KEYF, SSL_FILETYPE_PEM) <= 0) {
        ERR_print_errors_fp(stderr);
        exit(4);
    }

    if (!SSL_CTX_check_private_key(ctx)) {
        fprintf(stderr,"Private key does not match the certificate public key\n");
        exit(5);
    }


    // std::vector<int> clients; // client sockets
    std::vector<client> clients_struc;

    // server main loop
    while (1)
    {
        int sock_client = -1;
        int socket_maximum = -1;
        while (1) // wait for new client
        {
            // set for handles
            fd_set read_wait_set;
            // empty set
            FD_ZERO(&read_wait_set);
            // add stdin
            FD_SET(STDIN_FILENO, &read_wait_set);
            // add listen socket
            FD_SET(sock_listen, &read_wait_set);

            // find socket list maximum
            socket_maximum = sock_listen;
            for (int i = 0; i < clients_struc.size(); i++)
            {
                FD_SET(clients_struc[i].client_fd, &read_wait_set);
                if (clients_struc[i].client_fd > socket_maximum)
                {
                    socket_maximum = clients_struc[i].client_fd;
                }
            }

            // wait set of fd is always max fd + 1
            int sel = select(socket_maximum + 1, &read_wait_set, NULL, NULL, NULL);

            if (sel < 0)
            {
                log_msg(LOG_ERROR, "Select failed!");
                exit(1);
            }

            if (FD_ISSET(sock_listen, &read_wait_set))
            { // new client?
                
                sockaddr_in rsa;
                int rsa_size = sizeof(rsa);
                // new connection
                sock_client = accept(sock_listen, (sockaddr *)&rsa, (socklen_t *)&rsa_size);
                if (sock_client == -1)
                {
                    log_msg(LOG_ERROR, "Unable to accept new client.");
                    close(sock_listen);
                    exit(1);
                }
                // clients.push_back(sock_client);

                // obtain client ssl
                SSL* ssl;
                int err;
                ssl = SSL_new (ctx);                           
                // CHK_NULL(ssl);
                SSL_set_fd (ssl, sock_client);
                err = SSL_accept (ssl);                        
                // CHK_SSL(err);

                client new_client = {sock_client, ssl};
                clients_struc.push_back(new_client);

                printf("new client\n");

                client_cert = SSL_get_peer_certificate(ssl);
                if (client_cert != NULL)
                {
                    printf("Obtaining client certificate\n");
                }
                else
                {   
                    printf("No cetificate for client\n");
                }

                uint lsa = sizeof(srv_addr);
                // my IP
                getsockname(sock_client, (sockaddr *)&srv_addr, &lsa);
                log_msg(LOG_INFO, "My IP: '%s'  port: %d",
                        inet_ntoa(srv_addr.sin_addr), ntohs(srv_addr.sin_port));
                // client IP
                getpeername(sock_client, (sockaddr *)&srv_addr, &lsa);
                log_msg(LOG_INFO, "Client IP: '%s'  port: %d",
                        inet_ntoa(srv_addr.sin_addr), ntohs(srv_addr.sin_port));
            }

            char buf[128];
            if (FD_ISSET(STDIN_FILENO, &read_wait_set))
            {
                // read input
                int res_len = read(STDIN_FILENO, buf, sizeof(buf));
                for (int i = 0; i < clients_struc.size(); i++)
                {
                    if (res_len < 0)
                        log_msg(LOG_ERROR, "Unable to read data from stdin.");
                    else
                        log_msg(LOG_DEBUG, "Read %d bytes from stdin.", res_len);

                    // send input to client
                    // res_len = write(clients[i], buf, res_len);
                    res_len = SSL_write(clients_struc[i].ssl, buf, res_len);
                    if (res_len < 0)
                        log_msg(LOG_ERROR, "Unable to send data to client.");
                    else
                        log_msg(LOG_DEBUG, "Sent %d bytes to client.", res_len);
                }
            }
            // incoming data from clients
            for (int i = 0; i < clients_struc.size(); i++)
            {
                if (FD_ISSET(clients_struc[i].client_fd, &read_wait_set))
                {
                    // read data from socket
                    // int res_len = read(clients[i], buf, sizeof(buf));
                    int res_len = SSL_read(clients_struc[i].ssl, buf, sizeof(buf));
                    if (!res_len)
                    {
                        log_msg(LOG_DEBUG, "Client closed socket!");
                        close(clients_struc[i].client_fd);
                        SSL_free(clients_struc[i].ssl);
                        
                        clients_struc.erase(clients_struc.begin() + i);
                    }
                    else if (res_len < 0)
                        log_msg(LOG_DEBUG, "Unable to read data from client.");
                    else
                        log_msg(LOG_DEBUG, "Read %d bytes from client.", res_len);

                    // client close session
                    if (!strncasecmp(buf, "close", strlen(STR_CLOSE)))
                    {
                        log_msg(LOG_INFO, "Client sent 'close' request to close connection.");
                        close(clients_struc[i].client_fd);
                        clients_struc.erase(clients_struc.begin() + i);
                        log_msg(LOG_INFO, "Connection closed. Waiting for new client.");
                    }
                    else
                    {
                        // redistribute messages
                        res_len = write(STDOUT_FILENO, buf, res_len);
                        for (int ii = 0; ii < clients_struc.size(); ii++)
                        {
                            if (clients_struc[i].client_fd != clients_struc[ii].client_fd)
                            {
                                // res_len = write(clients[ii], buf, res_len);
                                res_len = SSL_write(clients_struc[ii].ssl, buf, res_len);
                            }
                        }
                        if (res_len < 0)
                        {
                            log_msg(LOG_ERROR, "Unable to write data to stdout.");
                        }

                    }

                }
            }
        } 
    }     

    return 0;
}
