#include <unistd.h>
#include <ctime>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <stdarg.h>
#include <sys/socket.h>
#include <sys/param.h>
#include <sys/time.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <netdb.h>

struct data
{
    int fd;
    char buff[255];
    int count;
    int status;
};

int readline2(data *connections, int nconnections, int len, int timeout)
{
    timeval tout = {timeout, 0};

    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(connections->fd, &fds);
    int counter = 0;

    while (1)
    {
        // find max fd
        int max_fd = connections[0].fd;
        for (int i = 0; i < nconnections; i++)
        {
            if (connections[i].fd > max_fd)
            {
                max_fd = connections[i].fd;
            }
        }
        printf("max file desc %d\n", max_fd);

        int sel = select(max_fd + 1, &fds, NULL, NULL, &tout);

        if (sel > 0)
        {
            for (int i = 0; i < nconnections; i++)
            {
                if (FD_ISSET(connections[i].fd, &fds))
                {
                    fcntl(connections[i].fd, F_SETFL, fcntl(connections[i].fd, F_GETFL) & O_NONBLOCK);
                    char ch;
                    if (read(connections[i].fd, &ch, 1) > 0)
                    {
                        printf("char from %d registered %c\n", connections[i], ch);
                        if (ch == 10 || connections[i].count >= len)
                        {
                            connections->buff[connections[i].count] = '\0';
                            connections[i].count++;
                            return 0;
                        }

                        connections->buff[connections[i].count] = ch;
                        connections[i].count++;
                    }
                    else
                    {

                    }
                }
            }
        }

        if (sel > 0)
        {
            char ch;
            if (read(connections->fd, &ch, 1) > 0)
            {
                printf("char registered %c\n", ch);
                if (ch == 10 || counter >= len)
                {
                    connections->buff[counter] = '\0';
                    counter++;
                    return 0;
                }

                connections->buff[counter] = ch;
                counter++;
            }
        }
        else if (sel == 0)
        {
            printf("Read line timeout\n");
            return 0;
        }
        else
        {
            printf("Read line Error\n");
            return 1;
        }

        usleep(100);
    }
    return 0;
}

int readline(int fd, data *buff, int len)
{
    while(1)
    {
        fcntl(fd, F_SETFL, fcntl(fd, F_GETFL) & O_NONBLOCK);
        char ch;
        int st = read(fd, &ch, 1);
        if (st > 0)
        {
            printf("char from %d registered %c\n", connections[i], ch);
            if (ch == 10 || connections->count >= len)
            {
                connections->buff[connections->count] = '\0';
                connections->count = 0;
                connections->status = st;
                return fd;
            }

            connections->buff[connections->count] = ch;
            connections->count++;
        }
        else if (st < 0)
        {
            connection->status = st;
            return fd;
        }
    }
}

int main()
{
    sockaddr_in inadr;
    inadr.sin_family = AF_INET;
    inadr.sin_port = htons(11111); // bind port 11111
    inadr.sin_addr.s_addr = INADDR_ANY;

    int sd = socket(AF_INET, SOCK_STREAM, 0);

    // reuse port 11111 when is still locked with previos process
    int opt = 1;
    if (setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) < 0)
    {
        printf("Unable to set socket option!\n");
    }

    // bind port and address
    int err = bind(sd, (sockaddr *)&inadr, sizeof(inadr));
    if (err < 0)
    {
        printf("bind failed\n");
    }

    // listen on port
    if (listen(sd, 1) < 0)
    {
        printf("cannot listen\n");
        close(sd);
        exit(1);
    }

    // wait for client
    sockaddr_in peeradr;
	socklen_t len_adr = sizeof( peeradr );
    int newsock = accept(sd, (sockaddr *)&peeradr, &len_adr);

    data con;
    // con.fd = newsock;
    printf("new conn %d\n", newsock);

    // readline(&con, 1, 50, 300);
    readline(newcon, &con, 50);

    if (con.status >= 0)
    {
        printf("%s\n", con.buff);

    }


    close(newsock);
    close(sd);
}