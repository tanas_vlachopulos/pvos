#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <stdarg.h>
#include <sys/socket.h>
#include <sys/param.h>
#include <sys/time.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <vector>
#include <semaphore.h>

struct file_lock
{
    char *filename;
    sem_t *sem;
};

int read_line(int fd, void *buffer, size_t n)
{
    int numRead;                    /* # of bytes fetched by last read() */
    int totRead;                     /* Total bytes read so far */
    char *buf;
    char ch;

    if (n <= 0 || buffer == NULL) {
        errno = EINVAL;
        return -1;
    }

    buf = (char*)buffer;                       /* No pointer arithmetic on "void *" */

    totRead = 0;
    for (;;) {
        numRead = read(fd, &ch, 1);

        if (numRead == -1) {
            if (errno == EINTR)         /* Interrupted --> restart read() */
                continue;
            else
                return -1;              /* Some other error */

        } else if (numRead == 0) {      /* EOF */
            if (totRead == 0)           /* No bytes read; return 0 */
                return 0;
            else                        /* Some bytes read; add '\0' */
                break;

        } else {                        /* 'numRead' must be 1 if we get here */
            if (totRead < n - 1) {      /* Discard > (n - 1) bytes */
                totRead++;
                *buf++ = ch;
            }

            if (ch == '\n')
                break;
        }
    }

    *buf = '\0';
    return totRead;
}

void client_child(int client_fd)
{
    printf("New connection\n");
    char data[256] = {0};
    char file_name[245] = {0};

    // int err = read(client_fd, data, sizeof(data));
    while(read_line(client_fd, data, sizeof(data)) > 0)
    {
        if (strstr(data, "GET") != NULL)
        {
            char* http_pos = strstr(data, "HTTP");
            memcpy(file_name, (strchr(data, '/') + 1), (http_pos - 1) - (strchr(data, '/') + 1));
            printf("filename: %s\n", file_name);
        }
        if (data[0] == '\n' || data[0] == '\r')
        {
            printf("end of request\n");
            break;
        }
    }

    if (file_name[0] != 0)
    {
        char response[255] = {0};

        int fp = open(file_name, O_RDONLY);

        if (fp < 0)
        {
            printf("Cannot read file\n");
            sprintf(response, "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\nConnection: Closed\r\n\r\n");
            write(client_fd, response, strlen(response));

            close(client_fd);
            exit(0);
        }

        // HTTP response
        sprintf(response, "HTTP/1.1 404 Not Found\r\nContent-Type: text/html\r\nConnection: Closed\r\n\r\n");
        write(client_fd, response, strlen(response));
        
        char w_buff[64];
        int ret = 0;
        while((ret = read(fp, w_buff, 64)) > 0)
        {
            int res = write(client_fd, w_buff, ret);
            if (res < 0)
            {
                printf("writing error\n");
            }
        }
        close(fp);

        printf("All data send. Closing socket.\n");
        close(client_fd);
        exit(0);
    }

    close(client_fd);
    exit(0);
}

int main()
{

    sockaddr_in inadr;
    inadr.sin_family = AF_INET;
    inadr.sin_port = htons(11111);
    inadr.sin_addr.s_addr = INADDR_ANY;

    int sd = socket(AF_INET, SOCK_STREAM, 0);

    int opt = 1;
    if (setsockopt(sd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt)) < 0)
    {
        printf("Unable to set socket option!\n");
    }

    int err = bind(sd, (sockaddr *)&inadr, sizeof(inadr));
    if (err < 0)
    {
        printf("bind selhal\n");
    }

    if (listen(sd, 1) < 0)
    {
        printf("nelze naslouchat\n");
        close(sd);
        exit(1);
    }

    struct timeval timeout;
    timeout.tv_sec = 30;
    timeout.tv_usec = 0;

    std::vector<file_lock> lock;    

    while (1)
    {
        fd_set wait_set;
        FD_ZERO(&wait_set);
        FD_SET(sd, &wait_set);

        int sel = select(sd + 1, &wait_set, NULL, NULL, NULL);

        if (FD_ISSET(sd, &wait_set))
        {
            sockaddr_in peeradr;
            socklen_t len_adr = sizeof(peeradr);
            int newsock = accept(sd, (sockaddr *)&peeradr, &len_adr);
            
            if (fork() == 0)
            {
                // server socket have to be closed for child
                close(sd);
                client_child(newsock);
            }
            else
            {
                // client socket have to be closed for parent
                close(newsock);
            }
        }

        usleep(10);
    }

    close(sd);
}